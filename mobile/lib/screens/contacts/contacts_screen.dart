import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/screens/camera/widgets/camera_body.dart';
import 'package:meals/screens/contacts/widgets/contacts_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class ContactsScreen extends StatelessWidget {
  static String routeName = "/contacts";

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments
    as ScreenArguments;

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: ContactsBody(),
      bottomNavigationBar: CustomBottomNavigation(3),
    );
  }
}
