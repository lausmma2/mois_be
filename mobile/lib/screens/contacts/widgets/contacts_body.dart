import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/widgets/default_button.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactsBody extends StatelessWidget {
  ContactsBody();

  void openFacebook({String url, String fallbackUrl}) async {


    try {
      bool launched =
      await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: SizeConfig.screenWidth * .85,
        child: Column(
          children: [
            Container(
                height: SizeConfig.screenHeightMinusAppBar * .45,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        image: DecorationImage(
                          image: AssetImage("assets/core/restaurant.jpg"),
                          alignment: Alignment.bottomCenter,
                          fit: BoxFit.fitWidth,
                        ),
                      )
                  ),
                )),
            SizedBox(
              height: SizeConfig.screenHeightMinusAppBar * .05,
            ),
            Container(
              height: SizeConfig.screenHeightMinusAppBar * .27,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: SizeConfig.screenHeightMinusAppBar * .27 * .5,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "+420 721 456 112",
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text(
                                "burger@emailcz",
                                style: Theme.of(context).textTheme.headline3,
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: SizeConfig.screenHeightMinusAppBar * .27 * .5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                             "Vedoucí:",
                              style: Theme.of(context).textTheme.headline3,
                            ),
                            Text(
                              "Tomáš Bůček",
                              style: Theme.of(context).textTheme.headline3,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  VerticalDivider(
                    color: Colors.grey,
                    thickness: 0.5,
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: SizeConfig.screenHeightMinusAppBar * .27 * .55,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Super burger",
                                style: Theme.of(context).textTheme.headline5,
                              ),
                              Text(
                               "Hradecká 1249/6",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              Text(
                                 "Hradec Králové",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: SizeConfig.screenHeightMinusAppBar * .27 * .4,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "IČO: 1115616616",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              Text(
                                "DIČ: 551156166",
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: SizeConfig.screenHeightMinusAppBar * .03,
            ),

            SignInButton(
              Buttons.Facebook,
              elevation: 5,
              padding: EdgeInsets.all(30),
              text: "Náš Facebook",
              onPressed: ()=>{
              openFacebook(fallbackUrl: "https://www.facebook.com/pizzapanda.cz",url: "fb://profile/445788768951766")
              },
            ),

          ],
        ),
      ),
    );
  }
}
