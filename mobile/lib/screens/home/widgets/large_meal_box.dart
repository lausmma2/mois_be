import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';

class LargeMealBox extends StatelessWidget {
  final Meal meal;

  const LargeMealBox({Key key, this.meal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {
        Navigator.of(context).pushNamed(
          MealDetailScreen.routeName,
          arguments: MealDetailScreenArguments(meal: meal),
        )
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 18.0, bottom: 8),
        child: Material(
          elevation: 3,
          shadowColor: Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.white,
              width: SizeConfig.screenWidth,
              height: SizeConfig.screenWidth * .45,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: SizeConfig.screenWidth * .41,
                    child: Stack(

                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      children: [(meal.image!=null)?
                        Image.memory(base64Decode(  meal.image),

                          height: SizeConfig.screenWidth * .45,
                          fit: BoxFit.fitHeight,
                        ):Container(),
                        Positioned(
                          left: 15,
                          top: 15,
                          child: Container(
                            decoration: BoxDecoration(color: AppColors.SUCCESS_COLOR, borderRadius: BorderRadius.all(Radius.circular(5))),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                              child: Text("${meal.price} Kč", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 15,
                          right: 15,
                          child: Container(
                            decoration: BoxDecoration(color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(5))),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(8, 6, 8, 6),
                              child: Text((meal.meatWeight==null)?"150 g":"${meal.meatWeight} g", style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          Text("${meal.name}", style: Theme.of(context).textTheme.bodyText1),
                          SizedBox(
                            height: 10,
                          ),
                          AutoSizeText(
                            "${meal.description}",
                            style: TextStyle(fontSize: 15),
                            minFontSize: 10,
                            maxLines: 2,
                            overflow: TextOverflow.fade,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
