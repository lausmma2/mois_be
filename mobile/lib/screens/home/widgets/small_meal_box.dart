import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/meal.dart';

class SmallMealBox extends StatelessWidget {
  final Meal meal;

  const SmallMealBox({Key key, this.meal}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8, bottom: 3),
      child: Material(
        elevation: 3,
        shadowColor: Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            width: SizeConfig.screenWidth * .32,
            height: SizeConfig.screenWidth * .4,
            color: Colors.white,
            child: Stack(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              children: [
                Positioned(
                  width: SizeConfig.screenWidth * .4,
                  height: SizeConfig.screenWidth * .4,
                  child: (meal.image != null) ? Image.memory(base64Decode(meal.image), fit: BoxFit.fill) : SmallMealBox(),
                ),
                Positioned(
                  left: 15,
                  top: 15,
                  child: Container(
                    // decoration: BoxDecoration(color: AppColors.SUCCESS_COLOR, borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 6, 8, 6),
                      child: Text("${meal.name}", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600)),
                    ),
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 40,
                  child: Container(
                    decoration: BoxDecoration(color: AppColors.SUCCESS_COLOR, borderRadius: BorderRadius.all(Radius.circular(3))),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(6, 4, 6, 4),
                      child: Text("${meal.price} Kč", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600)),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 15,
                  left: 15,
                  child: Container(
                    decoration: BoxDecoration(color: Colors.transparent, borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(6, 4, 6, 4),
                      child: Text((meal.meatWeight==null)?"150 g":"${meal.meatWeight} g", style: TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.w600)),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
