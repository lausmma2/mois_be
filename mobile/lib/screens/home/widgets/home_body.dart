import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/dummy_data.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/store/meal/meals/meals_bloc.dart';
import 'package:meals/screens/home/widgets/build_burger.dart';
import 'package:meals/screens/home/widgets/small_meal_box.dart';

import 'large_meal_box.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  MainPageIndex pageIndex = MainPageIndex.MENU;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Container(
              width: double.infinity,
              color: AppColors.SECONDARY_COLOR,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text(
                      "Dáme Burger",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  _buildStatsTabBar(),
                ],
              ),
            ),
          ),
          pageIndex == MainPageIndex.MENU ? _buildMenuPage() : _buildBuildBurger()
        ],
      ),
    );
  }

  _buildBuildBurger() {
    return BlocBuilder<MealsBloc, MealsState>(
      builder: (context, state) {
        if (state is MealsLoaded) {
          return BuildBurger(state.meals.first, false);
        }
        return Container(
            width: double.infinity,
            child: Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.grey,
            )));
      },
    );
  }

  List<Meal> mealsWithoutCustom(List<Meal> meals) {
    List<Meal> mealsOK = meals.cast();
    if (mealsOK != null && !mealsOK.isEmpty) mealsOK.removeAt(0);
    return mealsOK;
  }

  List<Widget> _burgerTabs(List<Meal> meals) {
    List<Widget> ret = [];
    for (int i = 1; i < meals.length; i++) {
      Meal meal = meals[i];
      ret.add(
        LargeMealBox(
          meal: meal,
        ),
      );
    }
    return ret;
  }

  Widget _buildMenuPage() {
    return Container(
      height: SizeConfig.screenHeightMinusAppBar * .85,
      color: AppColors.GREY_BACKGROUND,
      child: Padding(
        padding: const EdgeInsets.only(left: 25.0, right: 25),
        child: SingleChildScrollView(
          child: BlocBuilder<MealsBloc, MealsState>(
            builder: (context, state) {
              if (state is MealsLoaded) {
                return Column(crossAxisAlignment: CrossAxisAlignment.start, children: _burgerTabs(state.meals)
                    /*    Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Speciální nabídky",
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8, bottom: 16),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          SmallMealBox(
                            meal: state.meals.first,
                          ),
                          SmallMealBox(
                            meal: state.meals.first,
                          ),
                          SmallMealBox(
                            meal: state.meals.first,
                          ),
                        ],
                      ),
                    ),
                  ),
                  LargeMealBox(
                    meal: state.meals.first,
                  ),*/
                    );
              }
              return Container(
                  width: double.infinity,
                  child: Center(
                      child: CircularProgressIndicator(
                    backgroundColor: Colors.grey,
                  )));
            },
          ),
        ),
      ),
    );
  }

  Widget _buildStatsTabBar() {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: TabBar(
        indicator: BoxDecoration(
          color: AppColors.SECONDARY_COLOR,
          border: Border(
            bottom: BorderSide(
              color: AppColors.PRIMARY_COLOR,
              width: 3.0,
            ),
          ),
        ),
        indicatorColor: Colors.transparent,
        labelStyle: TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.w600,
        ),
        unselectedLabelColor: Colors.deepPurple,
        tabs: <Widget>[
          _buildBarItem('Menu'),
          _buildBarItem('Vytvoř si sám'),
        ],
        onTap: (index) {
          print(index);
          switch (index) {
            case 0:
              pageIndex = MainPageIndex.MENU;
              break;
            case 1:
              pageIndex = MainPageIndex.CREATE;
              break;
          }
          setState(() {});
        },
      ),
    );
  }

  Widget _buildBarItem(String label) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            label,
            style: Theme.of(context).textTheme.headline3,
          )),
    );
  }
}
