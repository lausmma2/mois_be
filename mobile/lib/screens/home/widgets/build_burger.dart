import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/store/meal/cart/cart_bloc.dart';

class BuildBurger extends StatefulWidget {
  final Meal meal;
  final bool edit;

  const BuildBurger(this.meal, this.edit);

  @override
  State<BuildBurger> createState() => _BuildBurgerState();
}

class _BuildBurgerState extends State<BuildBurger> {
  bool _showIngredients = false;
  bool minimalize = false;
  bool moveToCart = false;
  int ingredientOrderIndex = 1;
  IngredientCategory chosenCategory = IngredientCategory.CHEESE;
  final int moveToCartDurationMs = 1300;
  List<Ingredient> ingredients = [];

  @override
  void initState() {
    ingredients = widget.meal.clone().ingredients;
    ingredientOrderIndex = countOfSelected().toInt() + 1;
    super.initState();
  }

  double countOfSelected() {
    return ingredients.where((element) => (element.chosen != null && element.chosen)).toList().length.toDouble();
  }

  double countOfAllIngredients() {
    return ingredients.length.toDouble();
  }

  List<AnimatedPositioned> _buildIngredients() {
    List<AnimatedPositioned> ingrs = [];
    int stepSize = minimalize ? 15 : 35;
    int firstStep = minimalize ? 35 : 60;
    double topIndexMax = firstStep + countOfSelected() * stepSize;
    double bottom = topIndexMax;

    ingrs.add(
      AnimatedPositioned(
        duration: Duration(milliseconds: moveToCart ? moveToCartDurationMs : 300),
        curve: minimalize ? Curves.easeOutBack : Curves.easeInOut,
        top: moveToCart
            ? 480
            : minimalize
                ? 30 + bottom
                : 15 + bottom,
        right: moveToCart ? -300 : 0,
        left: 0,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 300),
          curve: Curves.ease,
          child: SizedBox(
            child: Image.asset("assets/core/ingredients/bulka2.png", height: 80, width: 250),
          ),
        ),
      ),
    );
    ingredients.forEach((element) {
      ingrs.add(
        AnimatedPositioned(
          key: UniqueKey(),
          duration: Duration(milliseconds: moveToCart ? moveToCartDurationMs : 300),
          curve: minimalize ? Curves.easeOutBack : Curves.easeInOut,
          top: moveToCart
              ? 480
              : (element.chosen != null && element.chosen)
                  ? topIndexMax -= stepSize
                  : SizeConfig.screenHeight,
          right: moveToCart
              ? -300
              : (element.chosen != null && element.chosen)
                  ? 0
                  : 200,
          left: 0,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 300),
            curve: Curves.easeInOut,
            child: GestureDetector(
              onTap: () => _ingredientUnselected(element),
              child: SizedBox(
                child: (element.image == null)
                    ? Container()
                    : Image.memory(
                        base64Decode(element.image),
                        height: (element.chosen != null && element.chosen) ? 80 : 0,
                        width: (element.chosen != null && element.chosen) ? 250 : 0,
                      ),
              ),
            ),
          ),
        ),
      );
    });
    ingrs.add(
      AnimatedPositioned(
        duration: Duration(milliseconds: moveToCart ? moveToCartDurationMs : 300),
        curve: minimalize ? Curves.easeOutBack : Curves.easeInOut,
        top: moveToCart ? 480 : 0,
        right: moveToCart ? -300 : 0,
        left: 0,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 300),
          curve: Curves.easeInOut,
          child: SizedBox(
            child: Image.asset("assets/core/ingredients/bulka.png", height: 80, width: 250),
          ),
        ),
      ),
    );

    return ingrs;
  }

  add() async {
    setState(() {
      minimalize = true;
    });
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      moveToCart = true;
    });

    Meal m = widget.meal.clone();
    m.ingredients = ingredients;
    m.computeFinalPrice();

    BlocProvider.of<CartBloc>(context).add(AddToCart(m));
    ingredients = widget.meal.clone().ingredients;
    await Future.delayed(Duration(milliseconds: moveToCartDurationMs));
    setState(() {
      moveToCart = false;
      minimalize = false;
    });
  }

  Widget _buildPriceWidget() {
    Meal m = widget.meal.clone();
    m.ingredients = ingredients;
    m.computeFinalPrice();

    return Container(
      child: Text(
        "${m.finalPrice} Kč",
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.screenHeightMinusAppBar * .85,
      // color: Colors.amber,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              add();
            });
          },
          child: Icon(
            Icons.add_shopping_cart_outlined,
            color: Colors.black,
          ),
          backgroundColor: AppColors.SECONDARY_COLOR,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  height: countOfAllIngredients() * 40 + 150,
                  child: Stack(
                    children: _buildIngredients(),
                  ),
                ),
              ),
            ),
            // IconButton(
            //   icon: Icon(Icons.keyboard_arrow_up_outlined, size: 40),
            //   color: Colors.grey,
            //   onPressed: () => {add()},
            // ),

            Divider(
              thickness: 1,
            ),
            // _buildBottomMenu(),
            _buildIngredientsCard()
          ],
        ),
      ),
    );
  }

  sortIngredients() {
    setState(() {
      ingredients.sort((a, b) => a.orderIndex.compareTo(b.orderIndex));
    });
  }

  _ingredientSelectedToggle(Ingredient i) {
    if (i.chosen != null && i.chosen) {
      i.orderIndex = 0;
    } else {
      i.orderIndex = ingredientOrderIndex;
      ingredientOrderIndex += 1;
    }
    sortIngredients();

    i.chosen = !i.chosen;
    setState(() {});
  }

  _ingredientUnselected(Ingredient i) {
    setState(() {
      i.chosen = false;
      i.orderIndex = 0;

      sortIngredients();
    });
  }

  Widget _buildIngredientsCard() {
    return AnimatedSwitcher(
      duration: Duration(seconds: 1),
      child: Container(
        width: SizeConfig.screenWidth,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 5, 25, 10),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(child: _buildPriceWidget()),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(Icons.keyboard_arrow_up_outlined, size: 30),
                        color: Colors.grey,
                        padding: EdgeInsets.zero,
                        onPressed: () => {toggleShowIngredients()},
                      ),
                      Text(
                        "Upravit",
                        style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  Expanded(child: Container())
                ],
              ),
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                child: _showIngredients ? _buildCategoryButtonList() : Container(),
              ),
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                child: _showIngredients ? _buildIngredientsList() : Container(),
              ),
              // SizedBox(
              //   height: 15,
              // ),
              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: MaterialButton(
              //       child: Container(
              //         width: SizeConfig.screenWidth * .7,
              //         decoration: BoxDecoration(
              //           color: AppColors.SECONDARY_COLOR,
              //           borderRadius: BorderRadius.all(
              //             Radius.circular(5),
              //           ),
              //         ),
              //         child: Padding(
              //           padding: const EdgeInsets.all(15.0),
              //           child: Text(
              //             "Přidat do košíku",
              //             textAlign: TextAlign.center,
              //             style: TextStyle(fontSize: 22, color: Colors.black, fontWeight: FontWeight.w800),
              //           ),
              //         ),
              //       ),
              //       onPressed: () => {}),
              // )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildIngredientsList() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: filterByActive(),
      ),
    );
  }

  Widget _buildCategoryButtonList() {
    return Row(
      children: [
        _buildCategoryButton("Sýry", IngredientCategory.CHEESE, "assets/category/cheese.png"),
        _buildCategoryButton("Masa", IngredientCategory.MEAT, "assets/category/meat.png"),
        _buildCategoryButton("Zelenina", IngredientCategory.VEGETABLE, "assets/category/vegetable.png"),
        _buildCategoryButton("Omáčky", IngredientCategory.SAUCE, "assets/category/sauce.png"),
      ],
    );
  }

  void changeSelectedCategory(IngredientCategory ic) {
    setState(() {
      chosenCategory = ic;
    });
  }

  void toggleShowIngredients() {
    setState(() {
      _showIngredients = !_showIngredients;
      ingredients = ingredients;
    });
  }

  List<Widget> filterByActive() {
    return ingredients.where((i) => i.category == chosenCategory).toList().map((e) => ingredientToSelect(e)).toList();
  }

  Widget ingredientToSelect(Ingredient i) {
    return GestureDetector(
      onTap: () => _ingredientSelectedToggle(i),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 60,
          decoration: BoxDecoration(
            color: (i.chosen != null && i.chosen) ? Colors.amber.withAlpha(50) : Colors.white70,
            // border: Border.all(color: i.chosen ? AppColors.SECONDARY_COLOR : Colors.grey, width: 2),
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Column(
            children: [
              Image.memory(
                base64Decode(i.image),
                height: 40,
              ),
              Row(
                children: [
                  FittedBox(
                      child: Text(
                    i.name,
                    style: Theme.of(context).textTheme.bodySmall,
                  )),
                  Text(' ${i.price},-'),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool isCategorySelected(IngredientCategory category) {
    return chosenCategory == category;
  }

  Widget _buildCategoryButton(String name, IngredientCategory category, String image) {
    return GestureDetector(
      onTap: () => {changeSelectedCategory(category)},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              width: SizeConfig.screenWidth * .18,
              height: SizeConfig.screenWidth * .18,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: isCategorySelected(category) ? AppColors.SECONDARY_COLOR : Colors.grey, width: 2),
                borderRadius: BorderRadius.all(
                  Radius.circular(100),
                ),
              ),
              child: Center(
                child: Container(
                  width: SizeConfig.screenWidth * .13,
                  child: new Image.asset(image),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 12, color: Colors.black, fontWeight: FontWeight.w600),
              ),
            )
          ],
        ),
      ),
    );
  }
}
