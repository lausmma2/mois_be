import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

import 'widgets/home_body.dart';

class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class HomeScreen extends StatefulWidget {
  static String routeName = "/Home";
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();



  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,
      drawerScrimColor: Color.fromARGB(200, 35, 170, 255),
        bottomNavigationBar: CustomBottomNavigation(0),
      body: HomeBody(),
    );
  }
}
