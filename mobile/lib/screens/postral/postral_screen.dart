import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/screens/camera/widgets/camera_body.dart';
import 'package:meals/screens/postral/widgets/postral_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class PostralScreen extends StatelessWidget {
  static String routeName = "/postral";

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments
    as ScreenArguments;

    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
          goBack: true, label: "Dokončit nákup", icon: Icon(Icons.lunch_dining)),
      body: PostralBody(),
      bottomNavigationBar: CustomBottomNavigation(2),
    );
  }
}
