import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:meals/common/address_search.dart';
import 'package:meals/common/pay.dart';
import 'package:meals/common/place_service.dart';
import 'package:meals/config/app_config.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/common/storage_utils.dart';
import 'package:meals/data/cart_repository.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/models/order.dart';
import 'package:meals/data/models/payment.dart';
import 'package:meals/data/models/user.dart';
import 'package:meals/data/repositories/user_repository.dart';
import 'package:meals/data/store/meal/cart/cart_bloc.dart';
import 'package:meals/data/store/orders/orders_bloc.dart';
import 'package:meals/finish/dummy/finish_screen.dart';
import 'package:meals/screens/home/home_screen.dart';
import 'package:meals/screens/profile_screen/profile_screen.dart';
import 'package:meals/widgets/default_button.dart';
import 'package:meals/widgets/phone_number_form_field.dart';
import 'package:place_picker/place_picker.dart';
import 'dart:math';

import 'package:place_picker/uuid.dart';

class PostralBody extends StatefulWidget {
  PostralBody();

  @override
  _PostralBodyState createState() => _PostralBodyState();
}

class _PostralBodyState extends State<PostralBody> {
  final PayService payService = PayService();

  double totalPrice = 0;
  LocationResult result;
  bool distanceOK = false;
  bool _inputValid = false;
  final _controller = TextEditingController();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  Order order = Order();
  bool nameInvalid = false;
  bool phoneInvalid = false;
  bool emailInvalid = false;
  bool cityInvalid = false;
  bool streetInvalid = false;
  bool streetNumberInvalid = false;
  bool _failedToPay = false;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    totalPrice = CartRepository.computeTotalPrice();
    loadUserFromStorage();
  }

  loadUserFromStorage() async {
    User user = await StorageUtil.readUserData();

    order.name = user.displayName;
    order.email = user.email;
    order.customer = user;
    _nameController.text = user.displayName;
    _emailController.text = user.email;
  }

  /*Future<void> getAddressAndCheck() async {
    double lat = 50.2043000;
    double lon = 15.8294000;

    LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            PlacePicker(
              "AIzaSyAwVjoIlFAjgIyMxDvhg9sNlqAVeTDUMJc",
              displayLocation: LatLng(lat, lon),
            )));

    // Handle the result in your way
    double latFromPicker = result.latLng.latitude;
    double lonFromPicker = result.latLng.longitude;

    double distance = Geolocator.distanceBetween(lat, lon, latFromPicker, lonFromPicker); //in meters

    setState(() {
      this.result = result;
      distanceOK = distance < 10000;
    });
  }*/

  void onSuggestionClick(Place placeDetails) {
    setState(() {
      print(placeDetails);
    });
  }

  bool isEmail(String input) => EmailValidator.validate(input);

  bool checkPhone(String input) {
    String pattern = r'^[1-9][0-9]{8}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(input);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeightMinusAppBar,
        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              /*   Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Z technických důvodů podporujeme pouze platbu hotově při převzetí",
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),*/
              SizedBox(
                height: 60,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Kam to máme dovézt?",
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Divider(),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: TextField(
                  controller: _nameController,
                  onChanged: (value) {
                    setName(value);
                  },
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    icon: Container(
                      width: 10,
                      height: 10,
                      child: Icon(
                        Icons.person,
                        color: nameInvalid ? Colors.red : Colors.black,
                      ),
                    ),
                    hintText: "Celé jméno",
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: TextField(
                  controller: _emailController,
                  onChanged: (value) {
                    setEmail(value);
                  },
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    icon: Container(
                      width: 10,
                      height: 10,
                      child: Icon(
                        Icons.alternate_email,
                        color: emailInvalid ? Colors.red : Colors.black,
                      ),
                    ),
                    hintText: "Email",
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Icon(
                          Icons.phone,
                          color: phoneInvalid ? Colors.red : Colors.black,
                        ),
                        Text(
                          "+420",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: phoneInvalid ? Colors.red : Colors.black,
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    PhoneNumberFormField(
                      onChange: (value) => {setPhone(value)},
                    ),
                  ],
                ),
              ),
              /*   Container(
                margin: EdgeInsets.only(left: 20),
                child: TextField(
                  controller: _phoneController,
                  onChanged: (value) {
                    setPhone(value);
                  },
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                    icon: Container(
                      width: 10,
                      height: 10,
                      child: Icon(
                        Icons.phone,
                        color: Colors.black,
                      ),
                    ),
                    hintText: "Telefonní číslo",
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
                  ),
                ),
              ),*/
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextField(
                      controller: _controller,
                      readOnly: true,
                      onTap: () async {
                        // generate a new token here
                        final sessionToken = Uuid().generateV4();
                        final Suggestion result = await showSearch(
                          context: context,
                          delegate: AddressSearch(sessionToken),
                        );
                        // This will change the text displayed in the TextField
                        if (result != null) {
                          final placeDetails = await PlaceApiProvider(sessionToken).getPlaceDetailFromId(result.placeId);
                          setState(() {
                            _controller.text = result.description;

                            setAddress(placeDetails);
                          });
                        }
                      },
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        icon: Container(
                          width: 10,
                          height: 10,
                          child: Icon(
                            Icons.home,
                            color: (streetInvalid || cityInvalid || streetNumberInvalid) ? Colors.red : Colors.black,
                          ),
                        ),
                        hintText: "Adresa doručení s číslem popisným",
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Text(
                      'Adresa:',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: 'Ulice: ', style: new TextStyle(color: streetInvalid ? Colors.red : Colors.black, fontWeight: FontWeight.bold)),
                          new TextSpan(text: (order.street == null) ? "" : order.street),
                        ],
                      ),
                    ),
                    RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: 'Číslo popisné: ', style: new TextStyle(color: streetNumberInvalid ? Colors.red : Colors.black, fontWeight: FontWeight.bold)),
                          new TextSpan(text: (order.streetNumber == null) ? "" : order.streetNumber),
                        ],
                      ),
                    ),
                    RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent
                        style: new TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                        ),
                        children: <TextSpan>[
                          new TextSpan(text: 'Město: ', style: new TextStyle(color: cityInvalid ? Colors.red : Colors.black, fontWeight: FontWeight.bold)),
                          new TextSpan(text: (order.city == null) ? "" : order.city),
                        ],
                      ),
                    ),

                    // Text('$_'),
                  ],
                ),
              ),
              // Container(
              //     child: Text(
              //   (result == null)
              //       ? ""
              //       : ((distanceOK && result != null))
              //           ? "Tato adresa je v pořádku"
              //           : "Tato adresa je dále než 10km :(",
              //   style: distanceOK ? Theme.of(context).textTheme.headline3 : Theme.of(context).textTheme.subtitle1,
              // )),
              /*  Padding(
                padding: const EdgeInsets.all(8.0),
                child: DefaultButton(
                    text: "Vyberte adresu",
                    width: SizeConfig.screenWidth * .5,
                    press: () => getAddressAndCheck()),
              ),*/
              Expanded(child: Container()),
              Text(
                "Celková cena: ${BlocProvider.of<CartBloc>(context).totalValue} Kč",
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                "(obsahuje cenu za dopravu ${AppConfig.DELIVERY_COST} Kč)",
                style: Theme.of(context).textTheme.headline3,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DefaultButton(
                    text: "Zaplatit",
                    isEnabled: (_inputValid),
                    width: SizeConfig.screenWidth * .6,
                    press: () {
                      pay(order);
                      /*    CartRepository.removeAll();
                      Navigator.of(context).pushNamed(
                        FinishScreen.routeName,
                      );*/
                    }),
              ),
              _failedToPay
                  ? Text(
                      "Chyba platby",
                      style: TextStyle(color: Colors.red),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  void setName(String value) {
    order.name = value;
    inputChanged();
  }

  void setPhone(String value) {
    order.phone = value.replaceAll(" ", "");
    inputChanged();
  }

  void setEmail(String value) {
    order.email = value;
    inputChanged();
  }

  void setAddress(Place placeDetails) {
    print(placeDetails.toString());
    order.street = placeDetails.street;
    order.city = placeDetails.city;
    order.streetNumber = placeDetails.streetNumber;
    inputChanged();
  }

  void inputChanged() {
    setState(() {
      _inputValid = checkOrder();
    });
  }

  bool checkOrder() {
    bool ret = true;
    if (order.name == null || order.name.isEmpty || order.name.length < 1) {
      nameInvalid = true;
      ret = false;
    } else {
      nameInvalid = false;
    }
    if (order.phone == null || !checkPhone(order.phone)) {
      phoneInvalid = true;
      ret = false;
    } else {
      phoneInvalid = false;
    }
    if (order.email == null || !isEmail(order.email)) {
      emailInvalid = true;
      ret = false;
    } else {
      emailInvalid = false;
    }
    if (order.city == null || order.city.isEmpty || order.city.length < 1) {
      cityInvalid = true;
      ret = false;
    } else {
      cityInvalid = false;
    }
    if (order.street == null || order.street.isEmpty || order.street.length < 1) {
      streetInvalid = true;
      ret = false;
    } else {
      streetInvalid = false;
    }
    if (order.streetNumber == null || order.streetNumber.isEmpty || order.streetNumber.length < 1) {
      streetNumberInvalid = true;
      ret = false;
    } else {
      streetNumberInvalid = false;
    }
    return ret;
  }

  pay(Order order) async {
    double amount_total = BlocProvider.of<CartBloc>(context).totalValue;
    double amount_total_delivery = AppConfig.DELIVERY_COST;
    double amount_total_meal = amount_total - amount_total_delivery;
    String description = "";

    try {
      List<Meal> meals = BlocProvider.of<CartBloc>(context).meals;
      order.orderedFoods = meals;
      print(order.toJson());
      UserRepository userRepository = UserRepository();

      order = await userRepository.createOrder(order: order);

      Payment payment = payService.createPaymentObject(amount_total, amount_total_meal, amount_total_delivery, description);

      UsePaypal paypal = payService.processPay(
        payment,
        order,
        successFunc,
        errorFunc,
        cancelFunc,
      );

      Navigator.of(context).push(
        MaterialPageRoute(builder: (BuildContext context) => paypal),
      );
    } on Exception catch (e) {
      print(e);
      setState(() {
        _failedToPay = true;
      });
      return;
    }
  }

  void successFunc(Map params) {
    //je to ok, přesměrovat na ok stranku
    print('_$params');
    setState(() {
      _failedToPay = false;
    });
    BlocProvider.of<CartBloc>(context).add(CleanCart());
    BlocProvider.of<OrdersBloc>(context).add(LoadOrdersEvent());
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Úspěšně zaplaceno, zkontrolujte stav objednávky na svém profilu")));
    Navigator.of(context).pushNamed(ProfileScreen.routeName);
    Navigator.of(context).pushNamed(ProfileScreen.routeName);
    // Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
  }

  void cancelFunc(Map params) {
    // cancel, zustava na teto strance
  }

  void errorFunc(error) {
    setState(() {
      _failedToPay = true;
    });
  }
}
