import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/screens/category/widgets/burger_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class CategoryScreen extends StatelessWidget {
  static String routeName = "/category";
  static  String label = "";

  Widget getBody(BuildContext context){
    final arguments =
    ModalRoute.of(context).settings.arguments as ScreenArguments;

    switch (arguments.category) {
      case MealCategory.BURGER:
        label = "Burgery";
        return CategoryBurgerBody();
        break;

    }
    return CategoryBurgerBody();
  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
          goBack: true, label: label, icon: Icon(Icons.lunch_dining)),
      body:getBody(context),
      bottomNavigationBar: CustomBottomNavigation(0),
    );
  }
}
