import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/dummy_data.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/widgets/bottom_navigation.dart';

class CategoryBurgerBody extends StatelessWidget {
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  final List<Meal> meals = burgersDummy;

  CategoryBurgerBody();

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: meals.length,
      itemBuilder: (BuildContext context, int index) {
        Meal meal = meals[index];
        if(meal == null) print("error");
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed(
              MealDetailScreen.routeName,
              arguments: ScreenArguments(category: MealCategory.BURGER,meal: meal),
            );
          },
          child: Container(
            height: SizeConfig.screenWidth * .25,
            decoration: BoxDecoration(
                color: Colors.amber[100],
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage(meal.image),
                  height: SizeConfig.screenWidth * .25,
                  width: SizeConfig.screenWidth * .25,
                  fit: BoxFit.fitHeight,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(' ${meals[index].name}',style: Theme.of(context).textTheme.headline2,),
                    ),
                    Text(' ${meals[index].description}'),
                  ],
                ),
                Text(' ${meals[index].price} Kč',style: Theme.of(context).textTheme.headline3,),
              ],
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }
}
