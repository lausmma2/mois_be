import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/widgets/bottom_navigation.dart';


import 'widgets/login_body.dart';

class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class LoginScreen extends StatefulWidget {
  static String routeName = "/Login";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,
      // drawerScrimColor: Color.fromARGB(200, 35, 170, 255),
      // bottomNavigationBar: CustomBottomNavigation(0),
      body: LoginBody(),
    );
  }
}
