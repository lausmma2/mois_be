import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meals/common/google.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/common/storage_utils.dart';
import 'package:meals/data/models/user.dart';
import 'package:meals/data/store/user/user_bloc.dart';
import 'package:meals/screens/home/home_screen.dart';
import 'package:meals/screens/main/main_screen.dart';

class LoginBody extends StatefulWidget {
  @override
  _LoginBodyState createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: AppColors.GREY_BACKGROUND,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Vítejte",
                  style: Theme.of(context).textTheme.bodyText1,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "„Chci bojovat, protože to je to jediné, co mě odpoutá od pojídání hamburgerů. Kdybych nic nedělal, vyjedl bych snad celou planetu.",
                  style: Theme.of(context).textTheme.headline3,
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "— George Foreman vysloužilý americký profesionální boxer 1949",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                SignInButton(
                  Buttons.Google,
                  elevation: 5,
                  text: "Přihlásit přes Google",
                  onPressed: signIn,
                ),
              ],
            ),
          ),
          Positioned(
            top: -SizeConfig.screenWidth * .28,
            left: -SizeConfig.screenWidth * .25,
            child: Image.asset(
              "assets/core/meal/burger2.png",
              filterQuality: FilterQuality.high,
              height: SizeConfig.screenWidth * .8,
            ),
          ),
          Positioned(
            top: SizeConfig.screenWidth * .05,
            right: -SizeConfig.screenWidth * .5,
            child: Image.asset(
              "assets/core/meal/burger2.png",
              filterQuality: FilterQuality.high,
              height: SizeConfig.screenWidth * .8,
            ),
          ),
          Positioned(
            bottom: -SizeConfig.screenWidth * .23,
            left: -SizeConfig.screenWidth * .25,
            child: Image.asset(
              "assets/core/meal/burger3.png",
              filterQuality: FilterQuality.high,
              height: SizeConfig.screenWidth * .8,
            ),
          ),
          Positioned(
            bottom: SizeConfig.screenWidth * .05,
            right: -SizeConfig.screenWidth * .3,
            child: Image.asset(
              "assets/core/meal/burger4.png",
              filterQuality: FilterQuality.high,
              height: SizeConfig.screenWidth * .8,
            ),
          ),
        ],
      ),
    );
  }

   signIn() async {
    final User user = await GoogleSignInApi.login();
    if (user == null) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Přihlášení se nezdrařilo")));
    }
    BlocProvider.of<UserBloc>(context).add(LoginUserEvent(user));
  }
}
