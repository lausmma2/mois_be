import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/config/app_config.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/cart_repository.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/repositories/meal_repository.dart';
import 'package:meals/data/repositories/user_repository.dart';
import 'package:meals/data/store/meal/cart/cart_bloc.dart';
import 'package:meals/screens/camera/camera_screen.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/screens/postral/postral_screen.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/confirm_dialog.dart';
import 'package:meals/widgets/default_button.dart';
import 'package:shake/shake.dart';

class CartBody extends StatefulWidget {
  CartBody();

  @override
  _CartBodyState createState() => _CartBodyState();
}

class _CartBodyState extends State<CartBody> {
  double totalPrice = 0;
  int sale = 0;
  List<bool> _isOpen;

  @override
  void reassemble() {
    super.reassemble();
  }

  @override
  void didUpdateWidget(oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    int mealCount = BlocProvider.of<CartBloc>(context).meals.length;
    _isOpen = List.filled(mealCount, false);

    super.initState();
    sale = CartRepository.sale;
    computeTotalPrice();

    ShakeDetector detector = ShakeDetector.autoStart(onPhoneShake: () {
      setState(() {});
    });
    detector.startListening();
  }

  void computeTotalPrice() {
    /* totalPrice = 0;
    meals.forEach((burger) {
      totalPrice += burger.finalPrice;
    });
    totalPrice -= sale;
    if (totalPrice < 0) totalPrice = 0;*/
  }

  showConfirmDialogRemoveItem(Meal meal) {
    return (BuildContext context) => ConfirmDialog(() => {
          BlocProvider.of<CartBloc>(context).add(RemoveFromCart(meal)),
        });
  }

  showConfirmDialogCleanCart() {
    return (BuildContext context) => ConfirmDialog(() => {
          BlocProvider.of<CartBloc>(context).add(CleanCart()),
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CartBloc, CartState>(listener: (context, state) {
      print("+" + state.toString());
    }, builder: (context, state) {
      if (state is CartNotEmpty) {
        return Column(
          children: [
            Container(
                height: SizeConfig.screenHeightMinusAppBar * .75,
                child: Column(
                  children: [
                    SingleChildScrollView(
                      child: ExpansionPanelList(
                          expandedHeaderPadding: EdgeInsets.zero,
                          expansionCallback: (i, isOpen) {
                            setState(() {
                              _isOpen[i] = !isOpen;
                            });
                          },
                          children: [
                            for (Meal burger in state.meals)
                              ExpansionPanel(
                                  isExpanded: _isOpen[state.meals.indexOf(burger)],
                                  headerBuilder: (content, isOpen) {
                                    return Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Container(
                                        height: SizeConfig.screenWidth * .2,
                                        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5))),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            (burger.image == null)
                                                ? Container()
                                                : Image.memory(
                                                    base64Decode(burger.image),
                                                    fit: BoxFit.fill,
                                                    height: SizeConfig.screenWidth * .15,
                                                    width: SizeConfig.screenWidth * .15,
                                                  ),
                                            SizedBox(
                                              width: SizeConfig.screenWidth * 0.02,
                                            ),
                                            Container(width: SizeConfig.screenWidth * 0.40, child: Text('${burger.name}', style: Theme.of(context).textTheme.headline4)),
                                            Expanded(
                                              child: Container(),
                                            ),
                                            Text(' ${burger.finalPrice} Kč', style: Theme.of(context).textTheme.headline4),

                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                  body: _buildBurgerInfo(burger)),
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                ' doprava',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Text(
                                ' ${AppConfig.DELIVERY_COST} Kč',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                              SizedBox(
                                width: 65,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )),
            Expanded(child: Container()),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Center(
                  child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "celkem ",
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      Text(
                        '${BlocProvider.of<CartBloc>(context).totalValue} Kč',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ],
                  ),
                  if (sale != 0)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "sleva ",
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Text(
                          '-${sale} Kč',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ],
                    ),
                ],
              )),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.delete_forever_rounded,
                    size: 40,
                    color: Colors.black,
                  ),
                  onPressed: () => showDialog<String>(
                    context: context,
                    builder: showConfirmDialogCleanCart(),
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                DefaultButton(
                  text: "Pokračovat",
                  width: SizeConfig.screenWidth * .4,
                  press: () {
                    Navigator.of(context).pushNamed(PostralScreen.routeName);
                  },
                ),
              ],
            ),
          ],
        );
        ;
      }
      return Center(
        child: Text("Jsem tak prázdný :("),
      );
    });
  }

  chosenIngredients(Meal meal) {}

  Widget _buildBurgerInfo(Meal burger) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(' Složení:', style: Theme.of(context).textTheme.headline4),
                  for (var ingr in MealRepository.getChosenIngredients(burger))
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(ingr.name),
                        Text(' ${ingr.price},-'),
                      ],
                    )
                ],
              ),
            ),
            Row(
              children: [
                /*  Padding(
               padding: const EdgeInsets.fromLTRB(8,8,20,8),
               child: IconButton(
                 icon: Icon(
                   Icons.edit_note,
                   size: 40,
                   color: Colors.black,
                 ),
                 onPressed: () => showDialog<String>(
                   context: context,
                   builder: showConfirmDialogRemoveItem(burger),
                 ),
               ),
             ),*/
                Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
                  child: IconButton(
                    icon: Icon(
                      Icons.delete_forever_rounded,
                      size: 40,
                      color: Colors.black,
                    ),
                    onPressed: () => showDialog<String>(
                      context: context,
                      builder: showConfirmDialogRemoveItem(burger),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
