import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/screens/camera/widgets/camera_body.dart';
import 'package:meals/screens/cart/widgets/cart_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class CartScreen extends StatelessWidget {
  static String routeName = "/cart";

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments
    as ScreenArguments;

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
          goBack: false, label: "Košík", icon: Icon(Icons.lunch_dining)),
      body: CartBody(),
      bottomNavigationBar: CustomBottomNavigation(1),
    );
  }
}
