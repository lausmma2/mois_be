import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/screens/main/widgets/main_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class MainScreen extends StatelessWidget{
  static String routeName = "/main";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        goBack: false,
        label: "Naše nabídka",
        icon: Icon(Icons.lunch_dining)
      ),
      body: MainBody(),
      bottomNavigationBar: CustomBottomNavigation(0),
    );
  }
}