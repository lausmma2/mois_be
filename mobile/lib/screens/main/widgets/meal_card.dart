import 'package:flutter/material.dart';

class MealCard extends StatelessWidget {
  final double width;
  final Color bgColor;
  final String label;
  final ImageProvider image;

  MealCard(
      {@required this.width,
      @required this.bgColor,
      @required this.label,
      @required this.image});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Column(
        children: [
          Container(
            width: width,
            height: width,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: image,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
