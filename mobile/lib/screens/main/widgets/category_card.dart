import 'package:flutter/material.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/screens/category/category_screen.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/widgets/bottom_navigation.dart';

class CategoryCard extends StatelessWidget {
  final double width, height;
  final Color bgColor;
  final String label;
  final ImageProvider image;
  final MealCategory category;

  CategoryCard(
      {@required this.width,
      @required this.height,
      @required this.bgColor,
      @required this.label,
      @required this.image,
      @required this.category});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(
            CategoryScreen.routeName,
            arguments: ScreenArguments(category: category),
          );
        },
        child: Container(
          width: width,
          padding: EdgeInsets.all(20),
          margin: const EdgeInsets.only(left: 5.0, right: 5.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            color: bgColor,
            image: DecorationImage(
              image: image,
              alignment: Alignment.bottomCenter,
              fit: BoxFit.fitWidth,
            ),
          ),
          child: Text(
            label,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ));
  }
}
