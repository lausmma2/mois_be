import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/dummy_data.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/screens/main/widgets/category_card.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/widgets/bottom_navigation.dart';

import 'meal_card.dart';

class MainBody extends StatefulWidget {
  @override
  _MainBodyState createState() => _MainBodyState();
}

class _MainBodyState extends State<MainBody> {
  static String routeName = "/main";

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 10, 0, 3),
          child: Text(
            "Vyber si",
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 3, 0, 7),
          child: Text(
            "na co máš chuť",
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Container(
          height: SizeConfig.screenHeightMinusAppBar * .4,
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                CategoryCard(
                  width: 150,
                  bgColor: Color(0xffffddc2),
                  label: "Burgery",
                  category: MealCategory.BURGER,
                  image: AssetImage("assets/core/category/burger.png"),
                ),
                CategoryCard(
                  width: 150,
                  bgColor: Color(0xffe4efdf),
                  label: "Pizzy",
                  category: MealCategory.PIZZA,
                  image: AssetImage("assets/core/category/pizza.png"),
                ),
                CategoryCard(
                  width: 150,
                  bgColor: Color(0xffe4e3f1),
                  label: "Saláty",
                  category: MealCategory.SALAD,
                  image: AssetImage("assets/core/category/salad.png"),
                ),
                CategoryCard(
                  width: 150,
                  bgColor: Color(0xfff2efdf),
                  label: "Nápoje",
                  category: MealCategory.DRINKS,
                  image: AssetImage("assets/core/category/limo.png"),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 30, 0, 7),
          child: Text(
            "Nejoblíbenější jídla",
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Container(
          height: SizeConfig.screenHeightMinusAppBar * .25,
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                        MealDetailScreen.routeName,
                        arguments: ScreenArguments(category: MealCategory.BURGER, meal: burgersDummy[0]),
                      );
                    },
                    child: MealCard(
                      width: SizeConfig.screenHeightMinusAppBar * 0.15,
                      bgColor: Color(0xffffddc2),
                      label: burgersDummy[0].name,
                      image: AssetImage(burgersDummy[0].image),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                        MealDetailScreen.routeName,
                        arguments: ScreenArguments(category: MealCategory.BURGER, meal: burgersDummy[1]),
                      );
                    },
                    child: MealCard(
                      width: SizeConfig.screenHeightMinusAppBar * 0.15,
                      bgColor: Color(0xffffddc2),
                      label: burgersDummy[1].name,
                      image: AssetImage(burgersDummy[1].image),
                    )),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
