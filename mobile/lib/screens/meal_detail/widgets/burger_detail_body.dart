import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/cart_repository.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/store/meal/cart/cart_bloc.dart';
import 'package:meals/widgets/default_button.dart';
import 'package:meals/widgets/extras_button.dart';

class BurgerDetailBody extends StatefulWidget {
  final Meal meal;

  BurgerDetailBody(this.meal);

  @override
  _BurgerDetailBodyState createState() => _BurgerDetailBodyState();
}

class _BurgerDetailBodyState extends State<BurgerDetailBody> {
  double totalPrice = 0;
  bool addingIntoCart = false;
  bool showAddedMessage = false;
  bool first = true;
  Meal meal;
  bool _showIngredients = false;
  IngredientCategory chosenCategory = IngredientCategory.CHEESE;

  @override
  void initState() {
    super.initState();
    meal = widget.meal.clone();
  }

  void computerTotalPrice() {
    setState(() {
      totalPrice = meal.computeFinalPrice();
    });
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: SizeConfig.screenWidth,
      height: SizeConfig.screenHeight,
      child: SizedBox(
        child: Stack(
          children: [
            Positioned(
                child: Container(
              child:(meal.image==null)?Container(): Image.memory(
                base64Decode(meal.image),
                fit: BoxFit.cover,
                height: double.infinity,
                alignment: Alignment.center,
              ),
            )),
            Positioned(
              top: 30,
              left: 10,
              child: _buildTopBar(),
            ),
            Positioned(
              top: 100,
              child: SizedBox(
                width: SizeConfig.screenWidth,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: SizeConfig.screenWidth * .4,
                      child: Text(
                        "${meal.name}",
                        style: TextStyle(fontSize: 35, color: Colors.white, fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      width: SizeConfig.screenWidth * .4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "150 g",
                            style: TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.w500),
                          ),
                          Text(
                            "${meal.finalPrice} Kč",
                            style: TextStyle(fontSize: 35, color: AppColors.SECONDARY_COLOR, fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(bottom: 0, left: 0, child: _buildIngredientsCard())
          ],
        ),
      ),
    );
  }

  Widget _buildTopBar() {
    return Container(
      width: SizeConfig.screenWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: IconButton(
              icon: Icon(Icons.arrow_back_outlined, size: 30),
              color: Colors.white,
              onPressed: () => {Navigator.of(context).pop()},
            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.fromLTRB(16.0,16,50,15),
          //   child: IconButton(
          //     icon: Icon(
          //       Icons.favorite_border_outlined,
          //       size: 30,
          //     ),
          //     color: Colors.white,
          //     onPressed: () => {},
          //   ),
          // )
        ],
      ),
    );
  }

  Widget _buildIngredientsCard() {
    return AnimatedSwitcher(
      duration: Duration(seconds: 1),
      child: Container(
        width: SizeConfig.screenWidth,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Text(
                      "Ingredience",
                      style: TextStyle(fontSize: 22, color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.keyboard_arrow_up_outlined, size: 40),
                    color: Colors.grey,
                    onPressed: () => {toggleShowIngredients()},
                  ),
                  Text(
                    "Ingredience",
                    style: TextStyle(fontSize: 22, color: Colors.white, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                child: _showIngredients ? _buildCategoryButtonList() : Container(),
              ),
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 150),
                child: _showIngredients ? _buildIngredientsList() : Container(),
              ),
              SizedBox(
                height: 15,
              ),
              showAddedMessage?Text("Přidáno"):Container(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MaterialButton(
                    child: Container(
                      width: SizeConfig.screenWidth * .7,
                      decoration: BoxDecoration(
                        color: AppColors.SECONDARY_COLOR,
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          "Přidat do košíku",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 22, color: Colors.black, fontWeight: FontWeight.w800),
                        ),
                      ),
                    ),
                    onPressed: () => {add()}),
              )
            ],
          ),
        ),
      ),
    );
  }

  add() async {

    setState(() {
      showAddedMessage = true;
    });
    Meal m = meal.clone();
    m.computeFinalPrice();

    BlocProvider.of<CartBloc>(context).add(AddToCart(m));
    m = widget.meal.clone();

    await Future.delayed(Duration(seconds: 2));
    setState(() {
      showAddedMessage = false;
    });
  }

  void changeSelectedCategory(IngredientCategory ic) {
    setState(() {
      chosenCategory = ic;
    });
  }

  void toggleShowIngredients() {
    setState(() {
      _showIngredients = !_showIngredients;
    });
  }

  List<Widget> filterByActive() {
    return meal.ingredients.where((i) => i.category == chosenCategory).toList().map((e) => ingredientToSelect(e)).toList();
  }

  Widget ingredientToSelect(Ingredient i) {
    return Container(
      child: CheckboxListTile(
        contentPadding: EdgeInsets.only(left: 50, right: 50),

        title: SizedBox(
          width: 200,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                i.name,
                style: TextStyle(fontSize: 12, color: Colors.black, fontWeight: FontWeight.w600),
              ),
              Text(
                "${i.price} kč",
                style: TextStyle(fontSize: 12, color: Colors.black, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        ),
        value: i.chosen,
        onChanged: (newValue) {
          setState(() {
            i.chosen = newValue;
            meal.computeFinalPrice();
          });
        },
        // controlAffinity: ListTileControlAffinity.leading, //  <-- leading Checkbox
      ),
    );
  }

  Widget _buildIngredientsList() {
    return Column(
      children: filterByActive(),
    );
  }

  Widget _buildCategoryButtonList() {
    return Row(
      children: [
        _buildCategoryButton("Sýry", IngredientCategory.CHEESE, "assets/category/cheese.png"),
        _buildCategoryButton("Masa", IngredientCategory.MEAT, "assets/category/meat.png"),
        _buildCategoryButton("Zelenina", IngredientCategory.VEGETABLE, "assets/category/vegetable.png"),
        _buildCategoryButton("Omáčky", IngredientCategory.SAUCE, "assets/category/sauce.png"),
      ],
    );
  }

  bool isCategorySelected(IngredientCategory category) {
    return chosenCategory == category;
  }

  Widget _buildCategoryButton(String name, IngredientCategory category, String image) {
    return GestureDetector(
      onTap: () => {changeSelectedCategory(category)},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              width: SizeConfig.screenWidth * .18,
              height: SizeConfig.screenWidth * .18,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: isCategorySelected(category) ? AppColors.SECONDARY_COLOR : Colors.grey, width: 2),
                borderRadius: BorderRadius.all(
                  Radius.circular(100),
                ),
              ),
              child: Center(
                child: Container(
                  width: SizeConfig.screenWidth * .13,
                  child: new Image.asset(
                    image,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 12, color: Colors.black, fontWeight: FontWeight.w600),
              ),
            )
          ],
        ),
      ),
    );
  }
}
