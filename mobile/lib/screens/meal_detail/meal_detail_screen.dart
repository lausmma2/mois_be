import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/screens/meal_detail/widgets/burger_detail_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class MealDetailScreen extends StatelessWidget {
  static String routeName = "/meal-detail";

  Widget getBody(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments as MealDetailScreenArguments;

    return BurgerDetailBody(arguments.meal);
  }

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      // appBar: CustomAppBar(goBack: true, label: "", icon: Icon(Icons.lunch_dining)),
      body: getBody(context),
      // bottomNavigationBar: CustomBottomNavigation(0),
    );
  }
}

class MealDetailScreenArguments {

  final int mealID;
  final Meal meal;

  MealDetailScreenArguments({this.mealID, this.meal});
}
