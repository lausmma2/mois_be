import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/screens/camera/widgets/camera_body.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class CameraScreen extends StatelessWidget {
  static String routeName = "/camera";

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context).settings.arguments
    as ScreenArguments;

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
          goBack: false, label: "Naskenovat slevový kód", icon: Icon(Icons.lunch_dining)),
      body: CameraBody(),
      bottomNavigationBar: CustomBottomNavigation(1),
    );
  }
}
