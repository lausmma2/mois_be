import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/cart_repository.dart';
import 'package:meals/widgets/default_button.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class CameraBody extends StatefulWidget {
  CameraBody();

  @override
  _CameraBodyState createState() => _CameraBodyState();
}

class _CameraBodyState extends State<CameraBody> {
  Barcode result;
  int sale = 0;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool flashLight = false;

  bool validateCode(String code) {
    if (code.startsWith("pizza")) return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(flex: 4, child: _buildQrView(context)),
        Expanded(
          flex: 1,
          child: FittedBox(
            fit: BoxFit.contain,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                if (result != null)
                  Text('Máte slevu: $sale Kč!')
                else
                  Text('Nenašel jsem žádný kód'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.all(8),
                        child: DefaultButton(
                          text: '${flashLight ? 'Vypnout' : 'Zapnout'} blesk',
                          width: SizeConfig.screenWidth * .35,
                          isEnabled: true,
                          press: () async {
                            flashLight = !flashLight;
                            await controller?.toggleFlash();
                            setState(() {});
                          },
                        )),
                    Container(
                        margin: EdgeInsets.all(8),
                        child: DefaultButton(
                          text: "Otočit kameru",
                          width: SizeConfig.screenWidth * .35,
                          isEnabled: true,
                          press: () async {
                            await controller?.flipCamera();
                            setState(() {});
                          },
                        ))
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        if (validateCode(scanData.code)) {
          result = scanData;
          String a = scanData.code.substring(5);
          sale = int.parse(a);
          CartRepository.sale = sale;
        } else {
          result = null;
        }
        print(result);
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
