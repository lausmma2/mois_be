import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/common/google.dart';
import 'package:meals/common/utils.dart';
import 'package:meals/config/app_config.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/order.dart';
import 'package:meals/data/models/user.dart';
import 'package:meals/data/store/orders/orders_bloc.dart';
import 'package:meals/data/store/user/user_bloc.dart';
import 'package:meals/screens/login_screen/login_screen.dart';
import 'package:meals/widgets/confirm_dialog.dart';
import 'package:meals/widgets/pull_to_refresh.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ProfileBody extends StatefulWidget {
  @override
  _ProfileBodyState createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  List<bool> _isOpen = [];

  void onRefresh(BuildContext context) async {
    OrdersState ordersState = BlocProvider.of<OrdersBloc>(context).state;
    if (!(ordersState is OrdersLoading)) BlocProvider.of<OrdersBloc>(context).add(LoadOrdersEvent());

    _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    OrdersState ordersState = BlocProvider.of<OrdersBloc>(context).state;
    if (!(ordersState is OrdersLoaded)) {
      BlocProvider.of<OrdersBloc>(context).add(LoadOrdersEvent());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UserBloc, UserState>(
      listener: (context, state) {
        if (state is UserInitial) {
          // Navigator.of(context).pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
        }
      },
      builder: (context, state) {
        print(state);
        if (state is UserLogged) {
          return _buildProfile(state.user);
        }
        return Container();
      },
    );
  }

  Widget _buildProfile(User u) {
    return PullToRefresh(
      refreshController: _refreshController,
      onRefresh: () => onRefresh(context),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Container(
          height: SizeConfig.screenHeightMinusAppBar,
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Text("Můj profil", style: Theme.of(context).textTheme.headline2),
              SizedBox(
                height: 15,
              ),
              Text(u.displayName == null ? "" : u.displayName, style: Theme.of(context).textTheme.headline5),
              Text(u.email == null ? "" : u.email),
              SizedBox(
                height: 40,
              ),
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Moje objednávky:",
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.start,
                  ),
                ),
              ),
              Container(
                height: SizeConfig.screenHeightMinusAppBar * .6,
                child: SingleChildScrollView(
                  child: BlocConsumer<OrdersBloc, OrdersState>(listener: (context, state) {
                    if (state is OrdersLoaded) {
                      _isOpen = List.filled(state.orders.length, false);
                    } else {
                      _isOpen = [];
                    }
                  }, builder: (context, state) {
                    if (state is OrdersLoaded) {
                      return _buildOrders(state.orders);
                    } else if (state is OrdersError) {
                      return _buildErrorOrders(state.message);
                    }
                    return _buildLoadingOrders();
                  }),
                ),
              ),
              Expanded(child: Container()),
              Divider(),
              Container(
                height: SizeConfig.screenHeightMinusAppBar * .1,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(primary: Colors.white, onPrimary: Colors.black, minimumSize: Size(double.infinity, 50)),
                  icon: Icon(
                    Icons.logout,
                    color: Colors.red,
                  ),
                  label: Text("Odhlásit"),
                  onPressed: () => showDialog<String>(
                    context: context,
                    builder: showConfirmDialogCleanCart(),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Color getColorByState(OrderStatus status) {
    Color color = Colors.white;

    switch (status) {
      case OrderStatus.DELIVERING:
      case OrderStatus.PREPARED:
      case OrderStatus.PREPARING:
      case OrderStatus.ORDERED:
        // TODO: Handle this case.
        break;
      case OrderStatus.PREPARING:
        // TODO: Handle this case.
        break;

        // TODO: Handle this case.
        break;
      case OrderStatus.DELIVERING:
        // TODO: Handle this case.
        break;
      case OrderStatus.TAKENOVER:
      case OrderStatus.CANCELLED:
        color = Colors.orange;
        break;
    }

    return color;
  }

  showConfirmDialogCleanCart() {
    return (BuildContext context) => ConfirmDialog(() => {logout()});
  }

  Widget _buildOrders(List<Order> orders) {
    if (orders == null || orders.isEmpty) return _buildNoOrders();
    if(_isOpen == null ||_isOpen.isEmpty)
      _isOpen = List.filled(orders.length, false);
    return SingleChildScrollView(
      child: ExpansionPanelList(
          expansionCallback: (i, isOpen) {
            setState(() {
              _isOpen[i] = !isOpen;
            });
          },
          children: panels(orders)),
    );
  }

  List<ExpansionPanel> panels(List<Order> ords) {
    List<ExpansionPanel> list = [];
    for (int i = 0; i < ords.length; i++) {
      Order o = ords[i];
      list.add(_orderListItem(o, i));
    }

    return list;
  }

  Widget _buildNoOrders() {
    return Container(
      width: double.infinity,
      child: Center(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("Zatím nebylo nic objednáno"),
      )),
    );
  }

  Widget _buildErrorOrders(String err) {
    return Container(
      width: double.infinity,
      child: Text("Chyba při načítání"),
    );
  }

  Widget _buildLoadingOrders() {
    return Container(
        width: double.infinity,
        child: Center(
            child: CircularProgressIndicator(
          backgroundColor: Colors.grey,
        )));
  }

  Future logout() async {
    await GoogleSignInApi.logout();
    BlocProvider.of<UserBloc>(context).add(LogoutUserEvent());
    Navigator.of(context).pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
  }

  ExpansionPanel _orderListItem(Order o, int i) {
    return ExpansionPanel(
      isExpanded: _isOpen[i],
      headerBuilder: (BuildContext context, bool isExpanded) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  (o.deliveryAddress != null) ? o.deliveryAddress : "",
                  style: Theme.of(context).textTheme.headline5,
                ),
                Text(
                  (o.createdAt != null) ? Utils.dateTimeToUserReadString(o.createdAt) : "",
                ),
              ],
            ),
            Expanded(child: Container()),
            Text(
              (o.finalPrice != null) ? "${o.finalPrice},-" : "",
              style: Theme.of(context).textTheme.headline5,
            ),
          ],
        );
      },
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          child: Column(
            children: [
              Text(
                "Stav: ",
                style: Theme.of(context).textTheme.headline5,
              ),
              Text(getTextForOrderStatus(o.orderStatus)),
              Text(
                "Položky: ",
                style: Theme.of(context).textTheme.headline5,
              ),
              for (var meal in o.orderedFoods)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(meal.name),
                    Text(' ${meal.price},-'),
                  ],
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text("doprava ${AppConfig.DELIVERY_COST},-")],
              )
            ],
          ),
        ),
      ),
    );
  }
}
