
import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';

import 'widgets/xxx_body.dart';

class NavItem {
  final Widget body;
  final String assetSvg;
  final Text title;

  NavItem({
    @required this.body,
    @required this.assetSvg,
    @required this.title,
  });
}

class XXXScreen extends StatefulWidget {
  static String routeName = "/XXX";
  @override
  _XXXScreenState createState() => _XXXScreenState();
}

class _XXXScreenState extends State<XXXScreen> {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();



  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      key: _drawerKey,
      resizeToAvoidBottomInset: false,
      drawerScrimColor: Color.fromARGB(200, 35, 170, 255),
      appBar: AppBar(
      ),
      body: XXXBody(),
    );
  }
}
