import 'package:flutter/material.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/widgets/bottom_navigation.dart';
import 'package:meals/widgets/custom_app_bar.dart';

class FinishScreen extends StatelessWidget {
  static String routeName = "/finish";

  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context).settings.arguments as ScreenArguments;

    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
          goBack: true, label: "Hotovo", icon: Icon(Icons.lunch_dining)),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: SizeConfig.screenWidth,
            height: SizeConfig.screenHeight,
            decoration: BoxDecoration(
                color: Colors.amber[100],
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,

              children: [
                new Image.asset(
                  "assets/core/deliveryGuy.png",
                  fit: BoxFit.fitHeight,
                  height: 300,
                  width: 300,
                  alignment: Alignment.center,
                ),
                Text(
                  "Vaší objednávku jsme přijali a děláme na ní",
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1,
                ),   Text(
                  "očekávaný čas: 45 minut",
                  softWrap: true,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline3,
                ),

              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: CustomBottomNavigation(2),
    );
  }
}
