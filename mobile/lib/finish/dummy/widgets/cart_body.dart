import 'package:flutter/material.dart';
import 'package:meals/data/cart_repository.dart';
import 'package:meals/data/models/meal.dart';

class CartBody extends StatefulWidget {
  CartBody();

  @override
  _CartBodyState createState() => _CartBodyState();
}

class _CartBodyState extends State<CartBody> {
  List<Meal> meals;

  @override
  void initState() {
    super.initState();
    meals = CartRepository.meals;
  }

  @override
  Widget build(BuildContext context) {
    return Text("dummy");
  }
}
