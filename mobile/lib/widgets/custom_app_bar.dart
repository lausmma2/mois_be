import 'package:flutter/material.dart';
import 'package:meals/config/app_colors.dart';
import 'package:meals/config/size_config.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool goBack;
  final String label;
  final Icon icon;

  CustomAppBar({@required this.goBack, @required this.label, @required this.icon});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return PreferredSize(
      preferredSize: Size.fromHeight(SizeConfig.screenHeightMinusAppBar * .1),
      child: SafeArea(
        child: AppBar(
          backgroundColor: Colors.transparent,
          leading: _barButton(context),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(SizeConfig.appBarHeight);

  Widget _barButton(BuildContext context) {
    return goBack
        ? Container(
            height: double.infinity,
            width: SizeConfig.appBarHeight,
            decoration: BoxDecoration(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_rounded,
                    size: 40,
                    color: Colors.black,
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                  color: Colors.black,
                ),
                SizedBox(
                  width: 30,
                ),
                Expanded(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        right: SizeConfig.appBarHeight,
                      ),
                      child: Text(
                        label,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        : Container(
            width: double.infinity,
            child: Center(
              child: Text(
                label,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          );
  }
}
