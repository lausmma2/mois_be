import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/config/size_config.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/store/meal/cart/cart_bloc.dart';
import 'package:meals/screens/camera/camera_screen.dart';
import 'package:meals/screens/cart/cart_screen.dart';
import 'package:meals/screens/contacts/contacts_screen.dart';
import 'package:meals/screens/home/home_screen.dart';
import 'package:meals/screens/login_screen/login_screen.dart';
import 'package:meals/screens/main/main_screen.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/screens/profile_screen/profile_screen.dart';

class CustomBottomNavigation extends StatefulWidget {
  final initIndex;

  CustomBottomNavigation(this.initIndex);

  @override
  _CustomBottomNavigationState createState() => _CustomBottomNavigationState();
}

class _CustomBottomNavigationState extends State<CustomBottomNavigation> {
  int _selectedIndex = 0;

  @override
  void initState() {
    _selectedIndex = widget.initIndex;
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;

      String routeName = MainScreen.routeName;
      print(index);
      switch (index) {
        case 0:
          routeName = HomeScreen.routeName;
          break;
        case 1:
          routeName = CartScreen.routeName;
          break;
        case 2:
          routeName = ProfileScreen.routeName;
          break;
        case 3:
          routeName = ContactsScreen.routeName;
          break;
      }
      Navigator.of(context).pushNamed(
        routeName,
        arguments: ScreenArguments(index: index),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.bottomNavigationBarHeight,
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        elevation: 12,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt_outlined),
            label: 'Menu',
          ),
          BottomNavigationBarItem(
            icon: BlocBuilder<CartBloc, CartState>(builder: (context, state) {

              if(state is CartNotEmpty){
                int count = state.meals.length;
                return Badge(

                    badgeContent: Text(count.toString()),
                    child: Icon(Icons.shopping_cart));
              }
              return  Icon(Icons.shopping_cart);
            }),
            label: 'Košík',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline_outlined),
            label: 'Profil',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.phone),
            label: 'Kontakty',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        onTap: _onItemTapped,
      ),
    );
  }
}

class ScreenArguments {
  final int index;
  final int mealID;
  final Meal meal;
  final MealCategory category;

  ScreenArguments({this.mealID, this.category, this.index, this.meal});
}
