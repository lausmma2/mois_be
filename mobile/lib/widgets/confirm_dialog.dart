import 'package:flutter/material.dart';
import 'package:meals/widgets/default_button.dart';

class ConfirmDialog extends StatelessWidget {
  final Function fce;

  ConfirmDialog(this.fce);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.zero,
      title: Text(
        'Opravdu?',
        style: Theme.of(context).textTheme.headline4,
        textAlign: TextAlign.center,
      ),
      content: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _yesBtn(context),
            SizedBox(
              width: 10,
            ),
            _noBtn(context)
          ],
        ),
      ),
    );
  }

  Widget _yesBtn(BuildContext context) {
    return DefaultButton(
      text: "Ano",
      color: Colors.green.withAlpha(80),
      press: () => {
        Navigator.pop(context, 'OK'),
        fce(),
      },
      width:130,
    );
  }

  Widget _noBtn(BuildContext context) {
    return DefaultButton(
      text: "Ne",
      color: Colors.red.withAlpha(80),
      press: () => {Navigator.pop(context, 'Cancel')},
      width: 130,
    );
  }
}