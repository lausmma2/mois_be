import 'package:flutter/material.dart';

import 'package:flutter_masked_text/flutter_masked_text.dart';

class PhoneNumberFormField extends StatefulWidget {
  final Function onChange;
  final Function onSaved;

  PhoneNumberFormField({
    this.onChange,
    this.onSaved,
    Key key,
  }) : super(key: key);

  @override
  _PhoneNumberFormFieldState createState() => _PhoneNumberFormFieldState();
}

class _PhoneNumberFormFieldState extends State<PhoneNumberFormField> {
  static const String CZ_PHONE_NO_PREFIX = "+420";
  static const String CZ_PHONE_NO_MASK = "000 000 000";
  static const phoneNumberHint = "777 888 999";
  final controller = MaskedTextController(mask: CZ_PHONE_NO_MASK);
  String _oldValue = "";
  FocusNode pinFocusNode;
  int phoneLength = 0;

  @override
  void initState() {
    super.initState();
    pinFocusNode = FocusNode();
  }

  @override
  void dispose() {
    pinFocusNode.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: TextFormField(
          scrollPadding: EdgeInsets.zero,
          maxLength: CZ_PHONE_NO_MASK.length,
          focusNode: pinFocusNode,
          controller: controller,
          keyboardType: TextInputType.phone,
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
          ),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            border: InputBorder.none,
            hintText: 'telefonní číslo',
            counterText: "",

          ),
          onChanged: (value) => {
            setState(() {
              String valueWithoutSpaces = value.replaceAll(" ", "");
              phoneLength = valueWithoutSpaces.length;
            }),
            // odstraň mezeru při mazaní, když je na řadě
            if (_oldValue.length > value.length)
              {
                if (value.length > 0 &&
                    value.length - 1 == value.lastIndexOf(" "))
                  {controller.text = value.substring(0, value.length - 1)}
              },
            if (value.length >= CZ_PHONE_NO_MASK.length)
              {
                pinFocusNode.unfocus(),
              },
            widget.onChange(value),
            _oldValue = value
          },
          onSaved: (value) => {
            if (value.length >= CZ_PHONE_NO_MASK.length)
              {
                pinFocusNode.unfocus(),
              },
            widget.onSaved(value)
          },
        ),
      ),
    );
  }
}
