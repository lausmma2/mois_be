import 'package:flutter/material.dart';

class ExtrasButton extends StatelessWidget {
  final String text;
  final Function press;
  final double width;
  final bool chosen;

  const ExtrasButton({
    Key key,
    @required this.text,
    @required this.press,
    this.width = double.infinity,
    this.chosen = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: 50,
      child: FlatButton(
        splashColor: chosen ? Colors.amber[900] : Colors.transparent,
        color: chosen ? Colors.amber[900] : Colors.grey,
        onPressed:  press,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width),
        ),
        child: Text(
          text,
          style: TextStyle(
            fontSize: 15,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
