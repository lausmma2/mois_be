
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meals/config/app_colors.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PullToRefresh extends StatelessWidget {
  final Function onRefresh;
  final Widget body;
  final RefreshController refreshController;
  PullToRefresh({@required this.onRefresh, @required this.body,  @required this.refreshController});


  Widget headerOnline() {
    return WaterDropHeader(waterDropColor: AppColors.SECONDARY_COLOR);
  }


  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      header:  headerOnline(),
      controller: refreshController,
      onRefresh: onRefresh,
      child: body,
    );
  }
}
