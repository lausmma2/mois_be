import 'package:flutter/material.dart';
import 'package:meals/config/app_colors.dart';

class DefaultButton extends StatelessWidget {
  final String text;
  final Function press;
  final double width;
  final bool isEnabled;
  final Color color;
  final Color textColor;

  const DefaultButton({
    Key key,
    @required this.text,
    @required this.press,
    this.width = double.infinity,
    this.isEnabled = true,
    this.color = AppColors.SECONDARY_COLOR,
    this.textColor = Colors.black
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
              width: width,
              height: 50,
              child: FlatButton(
                splashColor: isEnabled ? color : Colors.transparent,
                color: isEnabled ? color: Colors.grey,
                onPressed: isEnabled ? press : () => {},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(
                  text,
                  style: TextStyle(
                    fontSize: 15,
                    color: textColor,
                  ),
                ),
              ),
            ),
    );
  }
}
