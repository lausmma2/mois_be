import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/config/theme.dart';
import 'package:meals/data/repositories/meal_repository.dart';
import 'package:meals/data/repositories/user_repository.dart';
import 'package:meals/data/store/meal/meals/meals_bloc.dart';
import 'package:meals/data/store/orders/orders_bloc.dart';
import 'package:meals/data/store/user/user_bloc.dart';
import 'package:meals/screens/login_screen/login_screen.dart';

import 'config/routes.dart';
import 'data/store/meal/cart/cart_bloc.dart';
import 'screens/home/home_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Paint.enableDithering = true;
  UserRepository userRepo = UserRepository();
  runApp(MultiBlocProvider(providers: [
    BlocProvider<UserBloc>(
      create: (context) => UserBloc(userRepo),
    ),
    BlocProvider<CartBloc>(
      create: (context) => CartBloc(),
    ),
    BlocProvider<OrdersBloc>(
      create: (context) => OrdersBloc(userRepo),
    ),
    BlocProvider<MealsBloc>(
      create: (context) => MealsBloc(MealRepository()),
    ),
  ], child: MaterialApp(home: MyApp())));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool splashDone = true;

  void buildAnimatedSplash() async {
    //případně další funkcionalita během splashe
    await Future.delayed(Duration(seconds: 2));

    setState(() {
      splashDone = true;
    });
  }

  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(CheckLoggedEvent());
    BlocProvider.of<MealsBloc>(context).add(LoadMealsEvent());
    BlocProvider.of<OrdersBloc>(context).add(LoadOrdersEvent());

    super.initState();
    buildAnimatedSplash();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
      title: 'Dáme burger',
      theme: theme(),
      home: _buildHome(),
      routes: Routes.getRoutes(),
    );
  }

  Widget _buildHome() {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserLogged) {
        return BlocBuilder<MealsBloc, MealsState>(
          builder: (context, state) {
            if (state is MealsLoaded) {
              return HomeScreen();
            }
            return SplashScreen();
          },
        );
      } else if (state is UserNotLoggedIn) {
        return LoginScreen();
      }
      return SplashScreen();
    });
  }
}

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: new Image.asset(
          "assets/core/loading.png",
          fit: BoxFit.fitWidth,
          height: 150,
          width: 250,
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
