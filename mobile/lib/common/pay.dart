import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:meals/config/app_config.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/models/order.dart';
import 'package:meals/data/models/payment.dart';

class PayService {
  Payment createPaymentObject(double amount_total, double amount_total_meal, double amount_total_delivery, String description) {
    Payment payment = Payment(amount_total: amount_total, description: description, amount_total_delivery: amount_total_delivery, amount_total_meal: amount_total_meal);

    return payment;
  }

  UsePaypal processPay(Payment payment, Order order, Function successFunc, Function errorFunc, Function cancelFunc) {
    //validace
    if (payment.amount_total == null || payment.amount_total <= 0) throw Exception("Chybná celková cena");

    if (payment.amount_total_delivery == null) throw Exception("Chybná cena dopravy");

    if (payment.amount_total_meal == null || payment.amount_total <= 0) throw Exception("Chybná cena jídla");

    if (order.orderedFoods.isEmpty) throw Exception("neobsahuje žádné položky");

    String successUrl = "https://" + AppConfig.SERVER_BASE_URL + "/foodOrder/api/v1/order/payment/success/${order.id}";
    String cancelUrl = "https://" + AppConfig.SERVER_BASE_URL + "/foodOrder/api/v1/order/payment/cancelled/${order.id}";

    return UsePaypal(
      sandboxMode: true,
      clientId: "AT0csge0-tUDLaAiSTK2E5frfgZ-P3sMBWcWsx6BSEMEu3cdmCPziIr7Z8RrgwcegRh2VpYvrHfcjUnG",
      secretKey: "EK1HMwKQviWpBU8KAHIGLjtkiFksBG-Ego5vRP0iOQ1uoxfHty7Gb0Fyc07LJXoBZAsyF9g9Tk5XLLOj",
      returnURL: successUrl,
      cancelURL: cancelUrl,
      transactions: [
        {
          "amount": {
            "total": '${payment.amount_total}',
            "currency": "CZK",
            "details": {"subtotal": '${payment.amount_total_meal}', "shipping": '${payment.amount_total_delivery}', "shipping_discount": 0}
          },
          "description": payment.description,
          "item_list": {
            "items": [
              for (var meal in order.orderedFoods) {"name": "${meal.name}", "quantity": 1, "price": '${meal.finalPrice}', "currency": "CZK"}
            ],
            "shipping_address": {"recipient_name": "${order.name}", "line1": "${order.street} ${order.streetNumber} ", "line2": "", "city": "${order.city} ", "country_code": "CZ", "postal_code": "", "phone": "${order.phone} ", "state": "Česká republika"},
          }
        }
      ],
      note: "Contact us for any questions on your order.",
      onSuccess: (Map params) async {
        successFunc(params);
      },
      onError: (error) {
        errorFunc(error);
      },
      onCancel: (params) {
        cancelFunc(params);
      },
    );
  }
}
