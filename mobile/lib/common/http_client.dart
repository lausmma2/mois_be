import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:meals/config/app_config.dart';

class HttpClient {

  static Future<http.Response> get(String path) async {
    print("GET" + path);
    /*  if(await AuthorizationRepository.isTokenExpired()){
      authorizationRepo.refreshToken();
    }*/
    // print(AppConfig.SERVER_BASE_URL);
    final Uri uri = Uri.http(AppConfig.SERVER_BASE_URL, path);

    Map<String, String> headers = await makeHeaders();

    http.Response response = await http.get(uri, headers: headers);
    validateResponse(response);

    return response;
  }

  static Future<Map<String, String>> makeHeaders() async {
    Map<String, String> headers = {
      'Content-Type': 'application/json',
    };
    // String token = await StorageUtil.read(Constant.TOKEN);
    // String token = "token";
    // if (token != null) headers["Authorization"] = token;
    return headers;
  }

  static Future<http.Response> post({String path, Map<String, dynamic> body, Map<String, dynamic> queryParameters}) async {
    print("post $path");
    /*   if(await AuthorizationRepository.isTokenExpired()){

      authorizationRepo.refreshToken();
    }*/
    final Uri uri = Uri.https(AppConfig.SERVER_BASE_URL, path, queryParameters);
    Map<String, String> headers = await makeHeaders();

    http.Response response = await http.post(uri, headers: headers, body: jsonEncode(body)).timeout(const Duration(seconds: 10));

    validateResponse(response);

    return response;
  }

  static void validateResponse(http.Response response) {
    int statusCode = response.statusCode;
    if (statusCode >= 100 && statusCode <= 199) {} else if (response.statusCode >= 200 && response.statusCode <= 299) {
      //OK
      return;
    } else if (response.statusCode >= 300 && response.statusCode <= 399) {} else if (response.statusCode >= 400 && response.statusCode <= 499) {
      //client error = OcValidationException
      // handle400(response);
    } else if (response.statusCode >= 500 && response.statusCode <= 599) {
      //server error = RuntimeException
      // handle500(response);
    }
  }
}
/*
  static void handle400(http.Response response) {
    OcValidationException ocValidationException = OcValidationException.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    throw (ocValidationException);
  }
}

void handle500(http.Response response) {
  RuntimeException runtimeException = RuntimeException(DEFAULT_ERROR_MESSAGE);
  throw runtimeException;
}

void throwOfflineException() {
  RuntimeException runtimeException = RuntimeException(DEFAULT_OFFLINE_ERROR_MESSAGE);
  throw runtimeException;
}*/
