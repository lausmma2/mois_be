
import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meals/data/models/user.dart';

class StorageUtil {
  static final FlutterSecureStorage  _storage =new FlutterSecureStorage();


  /* static FlutterSecureStorage getInstance(){
    if (_storage == null) {
      _storage = new FlutterSecureStorage();
    }
    return _storage;
  }*/

// Read value

  static Future<String> read(String key) async {
    return await _storage.read(key: key);
  }

// Read all values
  static Future<Map<String, String>> readAll() async {
    return await _storage.readAll();
  }

// Delete value
  static void delete(String key) async {
    await _storage.delete(key: key);
  }

// Delete all
  static void deleteAll() async {
    await _storage.deleteAll();
  }

// Write value
  static Future<void> write(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  static Future<User> readUserData() async{

    String value = await _storage.read(key: "user");
    if(value == null ||value=="") return null;
    return User.fromJson(json.decode(value));
  }




}
