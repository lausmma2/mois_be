import 'package:intl/intl.dart';

class Utils{


  static String dateTimeToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('dd.MM.yyyy HH:mm').format(dateTime);
  }

  static String timeToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('HH:mm').format(dateTime);
  }

  static String dateToUserReadString(DateTime dateTime) {
    if (dateTime == null) return "";
    return DateFormat('dd.MM.yyyy').format(dateTime);
  }

  static String dateTimeToJsonString(DateTime dateTime) {
    if (dateTime == null) return "";
    String a = DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
    return a;
  }

  static String dateTimeToInvoiceNameFormat(DateTime dateTime) {
    if (dateTime == null) return "";
    String a = DateFormat('dd-MM-yyyy').format(dateTime);
    return a;
  }

  static DateTime dateTimeFromJson(String dateTime) {
    if (dateTime == null) return null;
    try {
      return DateFormat('yyyy-MM-dd HH:mm').parse(dateTime);
    } catch (e) {
      try{
        return DateFormat('yyyy-MM-ddTHH:mm').parse(dateTime);
      }catch(e){
        try {
          return DateFormat('yyyy-MM-dd').parse(dateTime);
        } catch (e) {
          return null;
        }
      }

    }
  }
}