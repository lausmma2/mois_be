import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:maps_places_autocomplete/model/place.dart';
import 'package:maps_places_autocomplete/model/suggestion.dart';
import 'package:meals/data/models/user.dart';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

class GoogleSignInApi {
  static final _googleSignIn = GoogleSignIn();

  static Future<User> login() async {
    GoogleSignInAccount googleAcc = await _googleSignIn.signIn();

    if(googleAcc == null) return null;

    User user = User(
      id: googleAcc.id,
      email: googleAcc.email,
      displayName: googleAcc.displayName,
      photoUrl: googleAcc.photoUrl,
      serverAuthCode: googleAcc.serverAuthCode,
    );

    await googleAcc.authentication.then((googleKey) {
      user.accessToken = googleKey.accessToken;
      user.idToken = googleKey.idToken;
    });

    return user;
  }

  static Future<GoogleSignInAccount> logout() {
    _googleSignIn.disconnect();
    _googleSignIn.signOut();
  }

  static Future<User> isSigned() async{
    GoogleSignInAccount googleAcc = await _googleSignIn.signInSilently();

    if(googleAcc == null) return null;

    User user = User(
      id: googleAcc.id,
      email: googleAcc.email,
      displayName: googleAcc.displayName,
      photoUrl: googleAcc.photoUrl,
      serverAuthCode: googleAcc.serverAuthCode,
    );

    await googleAcc.authentication.then((googleKey) {
      user.accessToken = googleKey.accessToken;
      user.idToken = googleKey.idToken;
    }).catchError((err){
      print('inner error '+err.toString());
    });

    return user;
  }
}
