import 'package:flutter/material.dart';

import 'app_colors.dart';

ThemeData theme() {
  return ThemeData(


    scaffoldBackgroundColor: Colors.white,
    brightness: Brightness.light,
    textTheme: textTheme(),
    primaryColor: AppColors.PRIMARY_COLOR,
    primaryIconTheme: IconThemeData(color: AppColors.CUSTOM_COLOR_C),
    // cardColor: AppColors.CUSTOM_COLOR_C,
    secondaryHeaderColor: AppColors.SECONDARY_COLOR,
    dialogBackgroundColor: AppColors.BACKGROUND_GREY,
    disabledColor: AppColors.CUSTOM_COLOR_D,
    bottomNavigationBarTheme: bottomNavigationBarTheme(),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: AppColors.CUSTOM_COLOR_C),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.SECONDARY_COLOR)),
    ),

    // errorColor: Colors.red,
    // hintColor: AppColors.BLUE_COLOR,
    cardTheme: CardTheme(color: AppColors.CUSTOM_COLOR_D.withAlpha(100)),
    visualDensity: VisualDensity(horizontal: -4, vertical: -4),
    // pageTransitionsTheme: PageTransitionsTheme(
    //   builders: {TargetPlatform.iOS: FadeTransitionBuilder(), TargetPlatform.android: FadeTransitionBuilder()},
    // ),
    pageTransitionsTheme: PageTransitionsTheme(builders: {TargetPlatform.android: CupertinoPageTransitionsBuilder(),}),
  );
}

class FadeTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(_, __, animation, ___, child) => FadeTransition(opacity: AlwaysStoppedAnimation(1), child: child); //0-1 (0= začátek animace, 1 = konec animace)
}

BottomNavigationBarThemeData bottomNavigationBarTheme() {
  return BottomNavigationBarThemeData(
    backgroundColor: AppColors.PRIMARY_COLOR,
    selectedItemColor: AppColors.SECONDARY_COLOR,
    unselectedItemColor: Colors.white,
    // selectedLabelStyle: TextStyle(fontWeight: FontWeight.w300, fontSize: 10),
  );
}

TextTheme textTheme() {
  return TextTheme(
    bodyText1: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.w900, letterSpacing: 1.3),
    headline1: TextStyle(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w600, letterSpacing: 1.3),
    headline2: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900, letterSpacing: 1.3),
    headline3: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600, letterSpacing: 1.3),
    headline4: TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.w900,
    ),
    headline5: TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.w600,
    ),
    headline6: TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.w900,
    ),
    subtitle1: TextStyle(color: Colors.red, fontSize: 16, fontWeight: FontWeight.w600, letterSpacing: 1.3),
    caption: TextStyle(
      color: Colors.black,
      fontSize: 14,
      fontWeight: FontWeight.w600,
    ),
  );
}
