import 'package:flutter/cupertino.dart';
import 'package:meals/finish/dummy/finish_screen.dart';
import 'package:meals/screens/camera/camera_screen.dart';
import 'package:meals/screens/cart/cart_screen.dart';
import 'package:meals/screens/category/category_screen.dart';
import 'package:meals/screens/contacts/contacts_screen.dart';
import 'package:meals/screens/home/home_screen.dart';
import 'package:meals/screens/login_screen/login_screen.dart';
import 'package:meals/screens/main/main_screen.dart';
import 'package:meals/screens/meal_detail/meal_detail_screen.dart';
import 'package:meals/screens/postral/postral_screen.dart';
import 'package:meals/screens/profile_screen/profile_screen.dart';

class Routes {
  static Map<String, WidgetBuilder> getRoutes() {
    return {
      MainScreen.routeName: (context) => MainScreen(),
      HomeScreen.routeName: (context) => HomeScreen(),
      MealDetailScreen.routeName: (context) => MealDetailScreen(),
      CameraScreen.routeName: (context) => CameraScreen(),
      ContactsScreen.routeName: (context) => ContactsScreen(),
      CartScreen.routeName: (context) => CartScreen(),
      CategoryScreen.routeName: (context) => CategoryScreen(),
      PostralScreen.routeName: (context) => PostralScreen(),
      FinishScreen.routeName: (context) => FinishScreen(),
      LoginScreen.routeName: (context) => LoginScreen(),
      ProfileScreen.routeName: (context) => ProfileScreen(),
    };
  }
}
