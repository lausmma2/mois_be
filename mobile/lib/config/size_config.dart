import 'package:flutter/cupertino.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double screenHeightMinusAppBar;
  static Orientation orientation;
  static double statusBarHeight;
  static double bottomNavigationBarHeight;
  static const double appBarHeight = 0;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    orientation = _mediaQueryData.orientation;
    bottomNavigationBarHeight = screenHeight * .1;
    statusBarHeight = _mediaQueryData.padding.top;
    screenHeightMinusAppBar = screenHeight -
        appBarHeight -
        statusBarHeight -
        bottomNavigationBarHeight;
  }
}
