import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppConfig {
  // static const SERVER_BASE_URL = "192.168.43.153:8080";
  static const SERVER_BASE_URL = "7e79-195-113-118-51.eu.ngrok.io";
  static const double DELIVERY_COST = 29.0;
}
