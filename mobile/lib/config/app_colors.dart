import 'package:flutter/cupertino.dart';

class AppColors {
  static const Color DARK_BLUE_COLOR = Color(0xff053764);

  static const Color PRIMARY_COLOR = Color(0xFF0f0f0f);
  static const Color SECONDARY_COLOR = Color(0xffffd900);
  static const Color SUCCESS_COLOR = Color(0xFF74c200);
  static const Color GREY_BACKGROUND = Color(0xFFf5f5f5);
  static const Color CUSTOM_COLOR_C = Color(0xFF003844);
  static const Color CUSTOM_COLOR_D = Color(0xFFE8EEF1);
  static const Color CUSTOM_COLOR_E = Color(0xff00BFAF);

  static const Color SLAB_DARK_COLOR = Color(0xffdbf4ff);
  static const Color SLAB_LIGHT_COLOR = Color(0xffe7f8ff);
  static const Color SLAB_OFF_COLOR = Color(0xfff5f5f5);
  static const Color SLAB_ICON_COLOR = Color(0xff25a9e1);
  static const Color SLAB_ICON_OFF_COLOR = Color(0xffd1d1d1);
  static const Color TEXT_OFF_COLOR = Color(0xff999999);
  static const Color BACKGROUND_GREY = Color(0xfff1f1f1);
  static const Color DARK_TEXT_COLOR = Color(0xffFF173760);
}
