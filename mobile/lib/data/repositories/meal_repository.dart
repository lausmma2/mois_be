import 'dart:convert';

import 'package:http/http.dart';
import 'package:meals/common/http_client.dart';
import 'package:meals/data/dummy_data.dart';
import 'package:meals/data/models/meal.dart';

class MealRepository{
  static const String _GET_MEALS = "/food/api/v1/food/";

  static List<Ingredient> getChosenIngredients(Meal m){
    return m.ingredients.where((element) => element.chosen!=null &&element.chosen).toList();
  }

  Future<List<Meal>> loadMeals() async {
    Response r = await HttpClient.get(_GET_MEALS);
    List<dynamic> list = (jsonDecode(utf8.decode(r.bodyBytes)) as List);
    // List<dynamic> list = (jsonDecode(dummyJson) as List);

    return list.map((i) => Meal.fromJson(i)).toList();
  }

}