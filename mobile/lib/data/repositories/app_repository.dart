


import 'package:http/http.dart';
import 'package:meals/common/http_client.dart';


class AppRepository {
  static const String _GET_PING = "/ping";

  Future<bool> serverIsActive() async {

    try {
      Response r = await HttpClient.get(_GET_PING);
      bool isActive = r.statusCode == 200;
      return isActive;
    } catch (e) {
      return false;
    }
  }
}
