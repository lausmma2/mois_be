import 'dart:convert';

import 'package:http/http.dart';
import 'package:meals/common/http_client.dart';
import 'package:meals/common/storage_utils.dart';
import 'package:meals/data/models/order.dart';
import 'package:meals/data/models/user.dart';

class UserRepository {
  static const String _POST_CREATE_ORDER = "/foodOrder/api/v1/order";
  static const String _POST_CONFIRM_ORDER = "/fxxxxxxxxxxxx";
  static const String _GET_ORDERS = "/foodOrder/api/v1/order/customer";

  Future<User> userStored() async {
    User user = await StorageUtil.readUserData();
    return user;
  }

  Future<List<Order>> loadOrders() async {
    User u = await userStored();
    String url = _GET_ORDERS + "/" + u.id;
    Response r = await HttpClient.get(url);
    List<dynamic> list = (jsonDecode(utf8.decode(r.bodyBytes)) as List);

    return list.map((i) => Order.fromJson(i)).toList();
  }

  Future<Order> createOrder({Order order}) async {
    order.id = null;
    Response r = await HttpClient.post(
      path: _POST_CREATE_ORDER,
      body: order.toJson(),
    );
    int orderId = int.parse(r.body);
    order.id = orderId;
    return order;
  }
}
