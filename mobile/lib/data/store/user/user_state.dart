part of 'user_bloc.dart';

@immutable
abstract class UserState extends Equatable {}

class UserInitial extends UserState { final UniqueKey key;

UserInitial(this.key);
@override
List<Object> get props => [key];
}

class UserNotLoggedIn extends UserState {
  final UniqueKey key;
  UserNotLoggedIn(this.key);

  @override
  List<Object> get props => [key];
}

class UserChecking extends UserState {
  final UniqueKey key;

  UserChecking(this.key);

  @override
  List<Object> get props => [key];
}

class UserLogged extends UserState {
  final UniqueKey key;
  final User user;

  UserLogged(this.user, this.key);

  @override
  List<Object> get props => [user,key];
}
