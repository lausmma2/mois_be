import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meals/common/google.dart';
import 'package:meals/common/storage_utils.dart';
import 'package:meals/data/models/user.dart';
import 'package:meals/data/repositories/user_repository.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository userRepository;

  UserBloc(this.userRepository) : super(UserInitial(UniqueKey()));

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is LoginUserEvent) {
      yield* loginUser(event.user);
    } else if (event is LogoutUserEvent) {
      yield* logoutUser();
    } else if (event is CheckLoggedEvent) {
      yield* checkLogged();
    }
  }

  Stream<UserState> checkLogged() async* {
    yield UserChecking(UniqueKey());
    User user = await userRepository.userStored();
    if (user != null) {
      User isSigned = await GoogleSignInApi.isSigned();
      print("is signed? " + isSigned.toString());
      if (user.email == isSigned.email) {
        yield UserLogged(user,UniqueKey());
      } else {
        yield UserNotLoggedIn(UniqueKey());
      }
    } else {
      yield UserNotLoggedIn(UniqueKey());
    }
  }

  Stream<UserState> loginUser(User user) async* {

    await StorageUtil.write("user", json.encode(user.toJson()));

    yield UserLogged(user, UniqueKey());
  }

  Stream<UserState> logoutUser() async* {
    StorageUtil.delete("user");

    yield UserNotLoggedIn(UniqueKey());
  }
}
