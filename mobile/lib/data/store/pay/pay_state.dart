part of 'pay_bloc.dart';

@immutable
abstract class PayState {}

class PayInitial extends PayState {}
class PayOrderCreating extends PayState {}
class PayOrderCreated extends PayState {}
class PayOrderPaid extends PayState {}
class PayDone extends PayState {}
class PayError extends PayState {
  final String err;

  PayError(this.err);

}
