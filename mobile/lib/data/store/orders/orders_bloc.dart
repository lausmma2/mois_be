import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meals/data/models/order.dart';
import 'package:meals/data/repositories/user_repository.dart';
import 'package:meta/meta.dart';

part 'orders_event.dart';
part 'orders_state.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  final UserRepository userRepository;

  OrdersBloc(this.userRepository) : super(OrdersInitial());

  @override
  Stream<OrdersState> mapEventToState(
    OrdersEvent event,
  ) async* {
    if(event is LoadOrdersEvent){
      yield* loadOrders();
    }
  }

  Stream<OrdersState> loadOrders() async*{
    yield OrdersLoading();
    yield OrdersLoading();
    try{
     List<Order> orders =  await userRepository.loadOrders();
      yield OrdersLoaded(orders);
    } catch (e){
      yield OrdersError(e.toString());
    }
  }

}
