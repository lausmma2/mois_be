import 'dart:async';


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals/data/repositories/app_repository.dart';
import 'package:meta/meta.dart';

part 'connection_event.dart';

part 'connection_state.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionBlocState> {
  final AppRepository repo;

  ConnectionBloc(this.repo) : super(ConnectionInitial());

  @override
  Stream<ConnectionBlocState> mapEventToState(
    ConnectionEvent event,
  ) async* {
    if (event is CheckConnectionEvent) {
      yield* checkInternetAndReturnAppWidget();
    } else if (event is ChangeConnectionToOffline) {
      yield* changeConnectionToOffline();
    }
  }

  Stream<ConnectionBlocState> changeConnectionToOffline() async* {
    // yield ConnectionOffline();
  }

  Stream<ConnectionBlocState> checkInternetAndReturnAppWidget() async* {
    yield ConnectionChecking();
    String token = "token";

    print("token? $token");
    bool hasToken = token != null;

      //kontrola serveru

      bool isActive = await repo.serverIsActive();


      if (isActive) {
        //vše ok

        if(hasToken){
          yield ConnectionOnlineLoggedIn(); //todo obnova
        }else{
          yield ConnectionOnlineNotLoggedIn();
        }
      } else {
        //server není dostupný
        yield ConnectionOnlineServerDown();
      }

  }

}
