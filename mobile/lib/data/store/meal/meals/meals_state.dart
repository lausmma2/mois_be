part of 'meals_bloc.dart';

@immutable
abstract class MealsState {}

class MealsInitial extends MealsState {}

class MealsLoading extends MealsState{}
class MealsErrorState extends MealsState{}
class MealsLoaded extends MealsState{
  final List<Meal> meals;

  MealsLoaded(this.meals);

}
