import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/repositories/meal_repository.dart';
import 'package:meta/meta.dart';

part 'meals_event.dart';
part 'meals_state.dart';

class MealsBloc extends Bloc<MealsEvent, MealsState> {
  final MealRepository mealRepository;

  MealsBloc(this.mealRepository) : super(MealsInitial());

  @override
  Stream<MealsState> mapEventToState(
    MealsEvent event,
  ) async* {
    if(event is LoadMealsEvent){
      yield* loadMeals();
    }
  }

  Stream<MealsState> loadMeals() async*{
    yield MealsLoading();
    try{
      List<Meal> meals =  await mealRepository.loadMeals();
      yield MealsLoaded(meals);
    } catch (e){
      yield MealsErrorState();
    }
  }
}
