part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class AddToCart extends CartEvent {
  final Meal meal;

  AddToCart(this.meal);
}

class RemoveFromCart extends CartEvent {
  final Meal meal;

  RemoveFromCart(this.meal);
}

class CleanCart extends CartEvent {
  CleanCart();
}
