part of 'cart_bloc.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

class CartNotEmpty extends CartState {
  final List<Meal> meals;
  final double totalValue;

  CartNotEmpty(this.meals, this.totalValue);
}
