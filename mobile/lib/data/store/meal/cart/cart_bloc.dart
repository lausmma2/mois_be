import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:meals/config/app_config.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meta/meta.dart';

part 'cart_event.dart';

part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartInitial()){
    totalValue = AppConfig.DELIVERY_COST;
  }

  List<Meal> meals = [];
  double totalValue = 0;

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is AddToCart) {
      yield* addToCart(event.meal);
    } else if (event is RemoveFromCart) {
      yield* removeFromCart(event.meal);
    } else if (event is CleanCart) {
      yield* cleanCart();
    }
  }

  Stream<CartState> addToCart(Meal meal) async* {
    meals.add(meal);
    totalValue += meal.finalPrice;
    yield CartNotEmpty(meals,totalValue);
  }

  Stream<CartState> removeFromCart(Meal meal) async* {
    meals.remove(meal);
    if (meals == null || meals.isEmpty) {
      totalValue =AppConfig.DELIVERY_COST;;
      yield CartInitial();
    } else {
      totalValue-= meal.finalPrice;
      yield CartNotEmpty(meals,totalValue);
    }
  }

  Stream<CartState> cleanCart() async* {
    meals = [];
    totalValue =AppConfig.DELIVERY_COST;;
    yield CartInitial();
  }
}
