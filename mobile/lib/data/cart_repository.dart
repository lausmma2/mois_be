import 'dart:math';

import 'package:meals/config/app_config.dart';

import 'models/meal.dart';


class CartRepository {
  static List<Meal> meals = [];
  static int sale = 0;

  static void addMeal(Meal burger) {
    meals.add(burger);
  }

  static void removeBurger(Meal burger) {
    meals.remove(burger);
  }



  static void setSale(int s){
    sale =s;
  }

  static void removeAll(){
    meals = [];
  }

  static double computeTotalPrice(){
    double totalPrice = 0;
    meals.forEach((meal) {
      totalPrice += meal.finalPrice;
    });

    totalPrice -= sale;
    if (totalPrice < 0) totalPrice = 0;
    totalPrice +=AppConfig.DELIVERY_COST;
    return totalPrice;
  }
}
