enum MealCategory{
  BURGER,
  PIZZA,
  SALAD,
  DRINKS
}

enum MainPageIndex{
  MENU,
  CREATE
}

enum IngredientCategory{
  VEGETABLE,
  CHEESE,
  MEAT,
  SAUCE
}

IngredientCategory getCategoryEnumFromString(String enumString) {
  for (IngredientCategory element in IngredientCategory.values) {
    if ("IngredientCategory.$enumString" == element.toString()) {
      return element;
    }
  }
  return null;
}

enum OrderStatus{
  ORDERED, PREPARING, PREPARED, DELIVERING, TAKENOVER, CANCELLED



}

String getTextForOrderStatus(OrderStatus status) {
  switch(status){
    case OrderStatus.ORDERED:
      return "Objednáno";
    case OrderStatus.PREPARING:
      return "Připravuje se";
      break;
    case OrderStatus.PREPARED:
      return "Připraveno";
      break;
    case OrderStatus.DELIVERING:
      return "Deručeno";
      break;
    case OrderStatus.TAKENOVER:
      return "Převzato";
      break;
    case OrderStatus.CANCELLED:
      return "Zrušeno";
      break;
  }
  return "";
}
OrderStatus getOrderStatusEnumFromString(String enumString) {
  for (OrderStatus element in OrderStatus.values) {
    if ("OrderStatus.$enumString" == element.toString()) {
      return element;
    }
  }
  return null;
}