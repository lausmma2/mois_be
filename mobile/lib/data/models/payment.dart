import 'meal.dart';

class Payment{
  final double amount_total;
  final double amount_total_meal;
  final double amount_total_delivery;
  final String description;

  Payment({this.amount_total, this.amount_total_meal, this.amount_total_delivery, this.description});


}