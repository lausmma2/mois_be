import 'package:meals/common/utils.dart';
import 'package:meals/data/models/enums.dart';
import 'package:meals/data/models/meal.dart';
import 'package:meals/data/models/user.dart';

class Order {
  int id;
  String name;
  String email;
  String phone;
  String city;
  String street;
  String streetNumber;
  String deliveryAddress;
  User customer;
  List<Meal> orderedFoods;
  DateTime createdAt;
  double finalPrice;
  OrderStatus orderStatus;

  Order({this.createdAt,this.id,this.finalPrice,this.name, this.email, this.phone, this.city, this.street, this.streetNumber,this.orderedFoods,this.customer,this.orderStatus,this.deliveryAddress});

  static Order fromJson(dynamic body) {
    return Order(
      id: body["id"],
      name: body["name"],
      createdAt: Utils.dateTimeFromJson(body['createdAt']),
      email: body["email"],
      phone: body["phone"],
      city: body["city"],
      finalPrice: body["finalPrice"],
      street: body["street"],
      deliveryAddress: body["deliveryAddress"],
      streetNumber: body["streetNumber"],
      orderedFoods: mealsFromJson(body["orderedFoods"]),
      customer: User.fromJson(body["customer"]),
      orderStatus: getOrderStatusEnumFromString(body["status"]),
    );
  }
  static List<Meal> mealsFromJson(List<dynamic> body) {

    if (body == null) return [];
    return (body.map((i) => Meal.fromJson(i["food"])).toList());
  }

  Map<String, dynamic> toJson() => {
    "orderedFoods": orderedFoods.map((e) => e.toJson()).toList(),
    "deliveryAddress": _deliveryAddress(),
    "phone": phone,
    "id": id,
    "customer": customer!=null?customer.toJson():null
  };

  String _deliveryAddress(){
    return '${this.city} ${this.street} ${this.streetNumber}';
  }


}

