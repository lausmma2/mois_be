class User {
   String displayName;

   String email;

   String id;
   String accessToken;
   String idToken;

   String photoUrl;

   String serverAuthCode;

  User({this.displayName, this.email, this.id, this.photoUrl, this.serverAuthCode, this.accessToken, this.idToken});

  static User fromJson(Map<String, dynamic> json) {
    return User(
      displayName: json["displayName"],
      email: json["email"],
      id: json["id"],
      photoUrl: json["photoUrl"],
      serverAuthCode: json["serverAuthCode"],
      accessToken: json["accessToken"],
      idToken: json["idToken"],
    );
  }

  Map<String, dynamic> toJson() => {
        "displayName": displayName,
        "email": email,
        "serverAuthCode": serverAuthCode,
        "photoUrl": photoUrl,
        "id": id,
        "accessToken": accessToken,
        "idToken": idToken,
      };
}
