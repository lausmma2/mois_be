import 'dart:collection';

import 'dart:typed_data';

import 'package:flutter/material.dart';

import 'enums.dart';

class Meal {
  int foodId;
  String name;
  String description;
  String image;
  Uint8List imageBytes;
  double price;
  double finalPrice;
  int meatWeight;
  bool saved = false;
  Key key;
  List<Ingredient> ingredients;
  List<Ingredient> additionalIngredients;

  Meal({this.foodId, this.name, this.description, this.image, this.imageBytes, this.price, this.meatWeight, this.saved, this.finalPrice, this.ingredients = const [], this.key, this.additionalIngredients = const []}) {
    if (this.finalPrice == null) this.finalPrice = this.price;
    this.key = UniqueKey();
    if (this.saved == null) this.saved = false;
    computeFinalPrice();
  }

  static Meal fromJson(dynamic body) {
    return Meal(
        foodId: body["foodId"], name: body["name"], image: body["image"], price: body["price"], imageBytes: body["imageBytes"], meatWeight: body["meatWeight"], description: body["description"], finalPrice: 0, saved: true, key: UniqueKey(), ingredients: ingredientFromJson(body["ingredients"]));
  }

  static List<Ingredient> ingredientFromJson(List<dynamic> body) {
    if (body == null) return [];
    return (body.map((i) => Ingredient.fromJson(i)).toList());
  }

  Map<String, dynamic> toJson() => {"foodId": foodId, "ingredients": usedIngredients()};

  List<int> usedIngredients() {
    return ingredients.where((i) => (i.chosen != null && i.chosen)).map((e) => e.id).toList();
  }

  Meal clone() {
    return new Meal(
      foodId: this.foodId,
      name: this.name,
      image: this.image,
      price: this.price,
      imageBytes: this.imageBytes,
      meatWeight: this.meatWeight,
      finalPrice: this.finalPrice,
      saved: true,
      key: UniqueKey(),
      ingredients: this.ingredients.map((e) => e.clone()).toList(),
    );
  }

  double computeFinalPrice() {
    double finalPrice = price;

    for (var extra in this.ingredients) {
      if (extra.chosen != null && extra.chosen && extra.isAdditional != null && extra.isAdditional) finalPrice += extra.price;
    }
    this.finalPrice = finalPrice;
    return finalPrice;
  }
}

class Ingredient {
  int id;
  String name;
  double price;
  bool chosen;
  bool isAdditional;
  IngredientCategory category;
  String image; //fixme
  String imagePath;
  int orderIndex;
  Key key;

  Ingredient clone() {
    return new Ingredient(
      id: this.id,
      name: this.name,
      price: this.price,
      chosen: this.chosen,
      category: this.category,
      image: this.image,
      imagePath: this.imagePath,
      key: this.key,
      isAdditional: this.isAdditional,
    );
  }

  Ingredient({
    this.id,
    this.name,
    this.price,
    this.chosen,
    this.category,
    this.image,
    this.imagePath,
    this.key,
    this.orderIndex = 0,
    this.isAdditional = true,
  });

  static Ingredient fromJson(dynamic body) {
    return Ingredient(
      id: body["id"],
      category: getCategoryEnumFromString(body["type"]),
      imagePath: body["imagePath"],
      isAdditional: body["isAdditional"],
      name: body["name"],
      image: body["image"],
      price: body["price"],
      chosen: parseChosenFromJson(body["isAdditional"]),
      orderIndex: body["orderIndex"],
      key: UniqueKey(),
    );
  }

  static bool parseChosenFromJson(dynamic b) {
    bool isAdditional = b;

    return !isAdditional;
  }
}
