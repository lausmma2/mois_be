
import Axios from "@/axios/app-axios";

import { ORDER_STATUS } from "@/domain/OrderDomain"

const state = {
    orderList: [],
    ordersRefreshed: false,
};

const getters = {
    getOrderList: state => {
        return state.orderList;
    },
};

const mutations = {
  STORE_ORDER_LIST(state, orders) {
    state.orderList = orders;
  },
  SET_ORDER_LIST_REFRESH_STATE(state, refreshed) {
    state.ordersRefreshed = refreshed;
  },
};

const actions = {
 loadOrders({ commit }) {
    return new Promise((resolve, reject) => {
      commit("SET_ORDER_LIST_REFRESH_STATE", false);
      Axios.get("foodOrder/api/v1/order")
          .then(result => {
            let orders = result.data.map(o => {
              return { ...o, status: ORDER_STATUS[o.status] }
            })
              
            commit("STORE_ORDER_LIST", orders);
            commit("SET_ORDER_LIST_REFRESH_STATE", true);
            resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  updateOrderStatus(a, order) {
    return new Promise((resolve, reject) => {
      Axios.patch(`foodOrder/api/v1/order/${order.id}/status?status=${order.status}`)
        .then(() => {
            resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
