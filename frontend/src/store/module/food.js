
import Axios from "@/axios/app-axios";

import { toBase64 } from "@/common/util"

const state = {
    ingredients: [],
    foods: [],
};

const getters = {
    getAllFoods: state => {
        return state.foods;
    },
    getAllIngredients: state => {
        return state.ingredients;
    },
};

const mutations = {
    SET_ALL_FOODS(state, foods) {
        state.foods = foods;
    },
    SET_ALL_INGREDIENTS(state, ingredients) {
        state.ingredients = ingredients;
    }
};

const actions = {
    loadAllFoods({ commit }) { 
        return new Promise((resolve, reject) => {
            Axios.get("food/api/v1/food")
                .then(res => {
                
                    commit("SET_ALL_FOODS", res.data);
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    loadAllIngredients({ commit }) {
        return new Promise((resolve, reject) => {
            Axios.get("food/api/v1/ingredient")
                .then(res => {
                
                    commit("SET_ALL_INGREDIENTS", res.data);
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    updateFood(_, editedFood) {
        const copyFood = Object.assign({}, editedFood)
        copyFood.image = undefined;
        copyFood.ingredients = copyFood.ingredients.map(i => i.id);
        return new Promise((resolve, reject) => {
            Axios.put(`food/api/v1/food/${editedFood.foodId}`, copyFood)
                .then(() => {       
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    updateIngredience(_, editedIngredient) {
        const copyIngredient = Object.assign({}, editedIngredient)
        copyIngredient.image = undefined;
        return new Promise((resolve, reject) => {
            Axios.put(`food/api/v1/ingredient/${copyIngredient.id}`, copyIngredient)
                .then(() => {       
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    async saveNewFood(_, newFood) { 
        const copyFood = Object.assign({}, newFood)
        copyFood.foodId = undefined;
        copyFood.image = await toBase64(copyFood.image);
        return new Promise((resolve, reject) => {
            Axios.post(
                `food/api/v1/food`,
                copyFood,
                
            )
                .then(() => {       
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
    async saveNewIngredient(_, newIngredient) { 
        const copyIngredient = Object.assign({}, newIngredient)
        copyIngredient.id = undefined;
        copyIngredient.image = await toBase64(copyIngredient.image);
        return new Promise((resolve, reject) => {
            Axios.post(
                `food/api/v1/ingredient`,
                copyIngredient,
                
            )
                .then(() => {       
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
};

export default {
  state,
  getters,
  mutations,
  actions
};
