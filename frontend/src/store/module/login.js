
import router from "../../router";
import Axios from "@/axios/app-axios";

const state = {
  token: null,
};

const getters = {
  isLoggedIn: state => !!state.token,
};

const mutations = {
  AUTH_USER(state, token) {
    state.token = token;
  },
  LOGOUT_USER(state) {
    state.token = null;
  },
};

const actions = {
    login({ commit }, { username, password }) {
    return new Promise((resolve, reject) => {
      Axios.post(
        "user/api/v1/user/login",
        {
          username: username,
          password: password
        },
        {
          headers: {
            "Content-Type": "text/plain"
          }
        }
      )
        .then(({ headers }) => {
          const token = headers.authorization.replace("Bearer ", "");

          commit("AUTH_USER", token);
          localStorage.setItem("token", token);

          resolve();
        })
        .catch(err => {
          reject(err);
        });
    });
  },
  tryAutoLogin({ commit }) {
    const token = localStorage.getItem("token");
    if (!token) return;

    commit("AUTH_USER", token);
  },
  logout({ commit }) {
    return new Promise((resolve) => {
      commit("LOGOUT_USER");
      localStorage.removeItem("token");

      router.push("/login");
      resolve();
    });
  },

};

export default {
  state,
  getters,
  mutations,
  actions
};
