import Vue from 'vue'
import Vuex from 'vuex'

import login from './module/login'
import foodOrder from './module/foodOrder'
import food from './module/food'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    login,
    food,
    foodOrder,
  }
});
