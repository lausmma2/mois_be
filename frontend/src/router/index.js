import Vue from 'vue'
import VueRouter from 'vue-router'
import Page404 from "@/views/404View.vue"

import store from "../store/index";

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    meta: {
      layout: "basic",
      title: "Login",
    },
    beforeEnter: (to, from, next) => {
      if (!store.getters.isLoggedIn) {
        next();
      } else {
        next({
          name: "home"
        });
      }
    },
    component: () => import('../views/LoginView.vue')
  },
  {
    path: '/',
    name: 'home',
    meta: {
      requiresAuth: true,
      title: "Home",
    },
    component: () => import('../views/HomeView.vue'),
  },
  {
    path: '/manage-orders',
    name: 'manage-orders',
    meta: {
      requiresAuth: true,
      title: "Správa objednávek",
    },
    component: () => import('../views/ManageOrdersView.vue'),
  },
  {
    path: '/edit-food',
    name: 'edit-food',
    meta: {
      requiresAuth: true,
      title: "Správa Jídla",
    },
    component: () => import('../views/ManageFoodView.vue'),
  },
  {
    path: '/add-ingredient',
    name: 'add-ingredient',
    meta: {
      requiresAuth: true,
      title: "Přidat Ingredienci",
    },
    component: () => import('../views/AddIngredientView.vue'),
  },
  {
    path: "*",
    name: "page-404",
    meta: {
      layout: "basic",
      title: "404 Not Found",
    },
    component: Page404
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next();
      return;
    }
    next({ path: "/login" });
  } else {
    next();
  }
});

export default router
