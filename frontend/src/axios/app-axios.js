import store from "@/store/index";
import Axios from "axios";

import { BACKEND_URL } from "@/common/constant"

const instance = Axios.create({
  baseURL: BACKEND_URL
});

instance.interceptors.request.use(
  function(config) {
    if (store.getters.isLoggedIn)
      config.headers["Authorization"] = `Bearer ${store.state.login.token}`;
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

export default instance;
