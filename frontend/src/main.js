import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'
import "moment/locale/cs";

import BasicLayout from "./layout/BasicLayout";
import LoggedLayout from "./layout/LoggedLayout";

Vue.component("basic-layout", BasicLayout);
Vue.component("logged-layout", LoggedLayout);

Vue.config.productionTip = false

store.dispatch("tryAutoLogin");

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
