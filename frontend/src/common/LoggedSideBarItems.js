const foodManageRoutes = [
    {
        title: "Správa jídla",
        icon: "mdi-food-outline",
        url: "/edit-food"
    },
    {
        title: "Přidat ingredienci",
        icon: "mdi-food-apple-outline",
        url: "/add-ingredient"
    },
];

const orderRoute = {
    title: "Objednávky",
    icon: "mdi-clipboard-list-outline",
    url: "/manage-orders"
};

      
export {foodManageRoutes, orderRoute};