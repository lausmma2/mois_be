
export const ORDER_STATUS =
    Object.freeze({
        "ORDERED": "ORDERED",
        "PREPARING": "PREPARING",
        "PREPARED": "PREPARED",
        "DELIVERING": "DELIVERING",
        "TAKENOVER": "TAKENOVER",
        "CANCELLED": "CANCELLED"
    });

export const allOrderStatuses = [
    ORDER_STATUS.ORDERED,
    ORDER_STATUS.PREPARING,
    ORDER_STATUS.PREPARED,
    ORDER_STATUS.DELIVERING,
    ORDER_STATUS.TAKENOVER,
    ORDER_STATUS.CANCELLED
];

export class Order {
    orderId;
    orderStatus;
    isPaid;
    paidDate;

    constructor(orderId, orderStatus, isPaid, paidDate) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.isPaid = isPaid;
        this.paidDate = paidDate;
    }

    computeColorByStatus() {
        switch (this.orderStatus) { 
            case ORDER_STATUS.CANCELLED:
                return "error";
            case ORDER_STATUS.TAKENOVER:
                return "success";
            default:
                return "grey lighten-2";
        }
    }

}