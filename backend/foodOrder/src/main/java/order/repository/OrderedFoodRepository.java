package order.repository;

import order.domain.OrderedFood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderedFoodRepository extends JpaRepository<OrderedFood, Integer> {
}
