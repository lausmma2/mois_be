package order.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import order.domain.Order;
import order.domain.OrderStatus;
import order.dto.OrderDto;
import order.request.OrderRequest;
import order.service.OrderService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("api/v1/order")
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public Integer createOrder(@RequestBody OrderRequest orderRequest) {
        log.info("New order creation {}", orderRequest);
        var order = orderService.createOrder(orderRequest);
        return order.getId();
    }

    @GetMapping
    public List<OrderDto> getAllOrders() {
        log.info("Get all orders");
        List<Order> orders = orderService.findAllOrders();
        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order o : orders) {
            var food = orderService.computeOrderDto(o);
            orderDtos.add(food);
        }
        return orderDtos;
    }

    @GetMapping("/{orderId}")
    public OrderDto getOrderByID(@PathVariable Integer orderId) {
        log.info("Get order by ID");
        return orderService.computeOrderDto(orderId);
    }

    @GetMapping("/customer/{customerId}")
    public List<OrderDto> getOrdersByCustomerId(@PathVariable String customerId) {
        log.info("Get all orders by customer ID");
        List<Order> orders = orderService.findAllOrdersByCustomerId(customerId);

        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order o : orders) {
            var food = orderService.computeOrderDto(o);
            orderDtos.add(food);
        }
        return orderDtos;
    }

    @PutMapping("/{orderId}")
    public OrderDto updateOrderByID(@PathVariable Integer orderId, @RequestBody OrderRequest orderRequest) {
        log.info("Update order by ID");
        var order = orderService.updateOrder(orderId, orderRequest);
        return orderService.computeOrderDto(order);
    }

    @GetMapping("/payment/success/{orderId}")
    public OrderDto successPayment(@PathVariable Integer orderId) {
        log.info("Patch order by ID");
        var order = orderService.patchOrderPayment(orderId, true);
        return orderService.computeOrderDto(order);
    }

    @GetMapping("/payment/cancelled/{orderId}")
    public OrderDto cancelledPayment(@PathVariable Integer orderId) {
        log.info("Patch order by ID");
        var order = orderService.patchOrderPayment(orderId, false);
        return orderService.computeOrderDto(order);
    }

    @PatchMapping("/{orderId}/status")
    public OrderDto changeStatus(@PathVariable Integer orderId, @RequestParam OrderStatus status) {
        log.info("Patch order status by ID");
        var order = orderService.patchOrderStatus(orderId, status);
        return orderService.computeOrderDto(order);
    }
}
