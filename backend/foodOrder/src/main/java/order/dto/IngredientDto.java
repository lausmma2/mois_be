package order.dto;

import java.math.BigDecimal;

public record IngredientDto(
        Integer id,
        String name,
        String description,
        BigDecimal price,
        boolean isAvailable,
        boolean isAdditional,
        byte[] image
) {
}
