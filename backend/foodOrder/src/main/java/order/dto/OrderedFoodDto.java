package order.dto;

import java.util.List;

public record OrderedFoodDto(
        Integer foodId,
        List<Integer> ingredients
) {

}
