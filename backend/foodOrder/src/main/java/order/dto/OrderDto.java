package order.dto;

import order.domain.Customer;
import order.domain.OrderStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public record OrderDto(
        Integer id,
        Integer deliveryCost,
        OrderStatus status,
        BigDecimal finalPrice,
        String deliveryAddress,
        String phone,
        Date createdAt,
        Date paidAt ,
        CustomerDto customer,
        List<OrderedFoodOrderDto> orderedFoods
) {
}
