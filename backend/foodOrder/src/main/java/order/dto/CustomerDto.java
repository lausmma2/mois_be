package order.dto;

public record CustomerDto(
        String id,
        String displayName,
        String email,
        String photoUrl,
        String serverAuthCode
) {
}
