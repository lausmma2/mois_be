package order.dto;

import java.util.List;

public record OrderedFoodOrderDto(
        Integer id,
        List<IngredientDto> ingredients,
        FoodDto food
) {
}
