package order.request;

import order.domain.Customer;
import order.dto.OrderedFoodDto;

import java.util.List;

public record OrderRequest(
        List<OrderedFoodDto> orderedFoods,
        String deliveryAddress,
        String phone,
        Customer customer
) {
}
