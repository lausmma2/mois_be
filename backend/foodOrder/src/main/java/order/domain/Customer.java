package order.domain;

import lombok.Data;
import order.dto.CustomerDto;
import order.dto.OrderDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "Customer")
public class Customer {

    @Id
    private String id;

    @Column
    private String displayName;

    @Column
    private String email;

    @Column
    private String photoUrl;

    @Column
    private String serverAuthCode;

    @Column
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Order> orders = new ArrayList<>();

    public CustomerDto createDto() {
        return new CustomerDto(id, displayName, email, photoUrl, serverAuthCode);
    }
}
