package order.domain;

import lombok.Data;
import order.dto.OrderDto;
import order.dto.OrderedFoodOrderDto;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "Food_Order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer deliveryCost;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column
    private BigDecimal finalPrice;

    @Column(nullable = false)
    private String deliveryAddress;

    @Column(nullable = false)
    private String phone;

    @Column
    private Date paidAt;

    @ManyToOne
    private Customer customer;

    @Column
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    List<OrderedFood> orderedFoods = new ArrayList<>();

    @Column
    @CreationTimestamp
    private Date createdAt;

    @Column
    @UpdateTimestamp
    private Date updatedAt;

    public void addOrderedFood(OrderedFood orderedFood) {
        getOrderedFoods().add(orderedFood);
    }

    public OrderDto createDto(List<OrderedFoodOrderDto> orderedFoods) {
        return new OrderDto(id, deliveryCost, status, finalPrice, deliveryAddress, phone, createdAt, paidAt, customer.createDto(), orderedFoods);
    }
}
