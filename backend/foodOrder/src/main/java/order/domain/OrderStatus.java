package order.domain;

public enum OrderStatus {
    ORDERED, PREPARING, PREPARED, DELIVERING, TAKENOVER, CANCELLED
}
