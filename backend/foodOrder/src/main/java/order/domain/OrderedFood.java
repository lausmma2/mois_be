package order.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import order.dto.FoodDto;
import order.dto.IngredientDto;
import order.dto.OrderedFoodDto;
import order.dto.OrderedFoodOrderDto;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Ordered_food")
public class OrderedFood {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Integer foodId;

    @ManyToOne
    private Order order;

    @Column
    private String note;

    @Column
    private BigDecimal totalPrice;

    @ElementCollection
    private Collection<Integer> ingredientsIds = new ArrayList<>();

}
