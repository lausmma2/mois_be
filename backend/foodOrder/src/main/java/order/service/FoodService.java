package order.service;

import lombok.AllArgsConstructor;
import order.dto.FoodDto;
import order.dto.IngredientDto;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@Service
public class FoodService {

    private final RestTemplate restTemplate;
    private final String FOOD_URL = "http://localhost:8082/api/v1/";

    // TIP - poslat array ingredient IDs a vrátit pole ingredients
    @Cacheable("ingredient")
    public IngredientDto getIngredientById(Integer ingredientId) {

        return restTemplate.getForObject(
                FOOD_URL + "ingredient/{ingredientId}",
                IngredientDto.class,
                ingredientId
        );
    }

    @Cacheable("food")
    public FoodDto getFoodById(Integer foodId) {

        return restTemplate.getForObject(
                FOOD_URL + "food/{foodId}",
                FoodDto.class,
                foodId
        );
    }
}
