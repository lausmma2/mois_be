package order.service;

import order.domain.Customer;
import order.domain.Order;
import order.domain.OrderStatus;
import order.domain.OrderedFood;
import order.dto.IngredientDto;
import order.dto.OrderDto;
import order.dto.OrderedFoodDto;
import order.dto.OrderedFoodOrderDto;
import order.repository.CustomerRepository;
import order.repository.OrderRepository;
import order.repository.OrderedFoodRepository;
import order.request.OrderRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class OrderService {

    private final int DEFAULT_DELIVERY_COST = 29;
    private final OrderRepository orderRepository;
    private final OrderedFoodRepository orderedFoodRepository;
    private final CustomerRepository customerRepository;
    private final FoodService foodService;

    public Order createOrder(OrderRequest request) {
        var newOrder = new Order();
        newOrder.setDeliveryCost(DEFAULT_DELIVERY_COST);
        newOrder.setStatus(OrderStatus.ORDERED);
        newOrder.setDeliveryAddress(request.deliveryAddress());
        newOrder.setPhone(request.phone());

        orderRepository.save(newOrder);

        var priceCounter = BigDecimal.ZERO;
        for (OrderedFoodDto of : request.orderedFoods()) {
            var orderedFood = new OrderedFood();
            var finalPriceCounter = BigDecimal.ZERO;
            for (Integer ingredientId : of.ingredients()) {
                var ingredient = foodService.getIngredientById(ingredientId);

                assert ingredient != null;
                if (ingredient.isAdditional()) {
                    finalPriceCounter = finalPriceCounter.add(ingredient.price()); //get price of all ingredients
                }
                orderedFood.getIngredientsIds().add(ingredientId); // ingredientIds z DB
            }

            var food = foodService.getFoodById(of.foodId());

            orderedFood.setFoodId(of.foodId()); // foodId z Db
            orderedFood.setTotalPrice(food.price().add(finalPriceCounter));
            orderedFood.setOrder(newOrder);

            finalPriceCounter = finalPriceCounter.add(food.price());

            //priceCounter = priceCounter.add(finalPriceCounter).add(BigDecimal.valueOf(DEFAULT_DELIVERY_COST));
            priceCounter = priceCounter.add(finalPriceCounter);

            newOrder.addOrderedFood(orderedFood);

            orderedFoodRepository.save(orderedFood);
        }
        newOrder.setFinalPrice(priceCounter);

        Optional<Customer> customerDb = customerRepository.findById(request.customer().getId());
        var customer = customerDb.orElseGet(() -> customerRepository.save(request.customer()));
        newOrder.setCustomer(customer);

        return orderRepository.save(newOrder);
    }

    public List<Order> findAllOrders() {
        return orderRepository.findAllByOrderByCreatedAtDesc();
    }

    public List<Order> findAllOrdersByCustomerId(String customerId) {
        return orderRepository.findAllByCustomerId(customerId);
    }

    public OrderDto computeOrderDto(Integer id) {
        var order = orderRepository.getById(id);
        return computeOrderDto(order);
    }

    public OrderDto computeOrderDto(Order order) {
        List<OrderedFoodOrderDto> orderedFoodOrderDtos = new ArrayList<>();
        for (OrderedFood of : order.getOrderedFoods()) {
            var food = foodService.getFoodById(of.getFoodId());

            List<IngredientDto> ingredientDtos = new ArrayList<>();
            for (Integer ingredientId : of.getIngredientsIds()) {
                var ingredient = foodService.getIngredientById(ingredientId);
                ingredientDtos.add(ingredient);
            }

            var orderedFoodOrderDto = new OrderedFoodOrderDto(of.getId(), ingredientDtos, food);
            orderedFoodOrderDtos.add(orderedFoodOrderDto);
        }

        return order.createDto(orderedFoodOrderDtos);
    }

    public Order findOrderByID(Integer id) {
        return orderRepository.getById(id);
    }

    public Order updateOrder(Integer id, OrderRequest request) {
        var existingOrder = findOrderByID(id);

        existingOrder.setDeliveryCost(29);
        // existingOrder.setStatus(request.status());

        return orderRepository.save(existingOrder);
    }

    public Order patchOrderPayment(Integer id, boolean isPaid) {
        var existingOrder = findOrderByID(id);

        if (isPaid)
            existingOrder.setPaidAt(new Date());
        else {
            existingOrder.setPaidAt(null);
            existingOrder.setStatus(OrderStatus.CANCELLED);
        }

        return orderRepository.save(existingOrder);
    }

    public Order patchOrderStatus(Integer id, OrderStatus status) {
        var existingOrder = findOrderByID(id);

        existingOrder.setStatus(status);

        return orderRepository.save(existingOrder);
    }
}
