package food.request;

import food.domain.IngredientType;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

public record IngredientRequest(
        String name,
        BigDecimal price,
        String description,
        IngredientType type,
        Boolean isAvailable,
        Boolean isAdditional,
        MultipartFile image,
        List<Integer> foodIds
) {
}
