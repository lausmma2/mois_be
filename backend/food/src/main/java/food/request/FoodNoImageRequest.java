package food.request;

import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

public record FoodNoImageRequest(
        String name,
        String description,
        BigDecimal price,
        List<Integer> ingredients
) {
}