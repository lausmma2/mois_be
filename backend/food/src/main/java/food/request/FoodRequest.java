package food.request;

import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.List;

public record FoodRequest(
        String name,
        String description,
        BigDecimal price,
        String image,
        List<Integer> ingredients
) {
}