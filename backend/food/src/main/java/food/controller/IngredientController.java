package food.controller;

import food.dto.IngredientDto;
import food.request.IngredientRequest;
import food.service.IngredientService;
import food.domain.Ingredient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("api/v1/ingredient")
public class IngredientController {

    private final IngredientService ingredientService;

    @PostMapping(consumes = {"multipart/form-data"})
    public IngredientDto createIngredient(@ModelAttribute IngredientRequest ingredientRequest) {
        log.info("New ingredient creation {}", ingredientRequest);
        var ingredient = ingredientService.createIngredient(ingredientRequest);
        return ingredient.createDto();
    }

    @GetMapping
    public List<IngredientDto> getAllIngredients() {
        log.info("Get all ingredients");
        List<Ingredient> ingredients = ingredientService.findAllIngredients();
        List<IngredientDto> ingredientDtos = new ArrayList<>();
        for (Ingredient ing : ingredients) {
            var ingredient = ing.createDto();
            ingredientDtos.add(ingredient);
        }
        return ingredientDtos;
    }

    @GetMapping("/{ingredientId}")
    public IngredientDto getIngredientByID(@PathVariable Integer ingredientId) {
        log.info("Get ingredient by ID");
        var ingredient = ingredientService.findIngredientByID(ingredientId);
        return ingredient.createDto();
    }

    @PutMapping("/{ingredientId}")
    public IngredientDto updateIngredientByID(@PathVariable Integer ingredientId, @RequestBody IngredientRequest ingredientRequest) {
        log.info("Update ingredient by ID");
        var ingredient = ingredientService.updateIngredient(ingredientId, ingredientRequest);
        return ingredient.createDto();
    }
}
