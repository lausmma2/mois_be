package food.controller;

import food.domain.Food;
import food.dto.FoodDto;
import food.request.FoodNoImageRequest;
import food.request.FoodRequest;
import food.service.FoodService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("api/v1/food")
public class FoodController {

    private final FoodService foodService;

    @PostMapping
    public FoodDto createFood(@RequestBody FoodRequest foodRequest) {
        log.info("New food creation {}", foodRequest);
        var food = foodService.createFood(foodRequest);
        return food.createDto();
    }

    @GetMapping
    public List<FoodDto> getAllFoods() {
        log.info("Get all foods");
        List<Food> foods = foodService.findAllFoods();
        List<FoodDto> foodDtos = new ArrayList<>();
        for (Food f : foods) {
            var food = f.createDto();
            foodDtos.add(food);
        }
        return foodDtos;
    }

    @GetMapping("/{foodId}")
    public FoodDto getFoodByID(@PathVariable Integer foodId) {
        log.info("Get food by ID");
        var food = foodService.findFoodByID(foodId);
        return food.createDto();
    }

    @PutMapping(value = "/{foodId}")
    public FoodDto updateFoodByID(@PathVariable Integer foodId, @RequestBody FoodNoImageRequest foodRequest) {
        log.info("Update food by ID");
        var food = foodService.updateFood(foodId, foodRequest);
        return food.createDto();
    }
}
