package food.repository;

import food.domain.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface FoodRepository extends JpaRepository<Food, Integer> {
    List<Food> findAllByOrderByIdAsc();
}
