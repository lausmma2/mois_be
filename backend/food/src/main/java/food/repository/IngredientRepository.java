package food.repository;

import food.domain.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Integer> {
}
