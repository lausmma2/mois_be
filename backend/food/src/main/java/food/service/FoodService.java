package food.service;

import food.domain.Food;
import food.domain.Ingredient;
import food.repository.FoodRepository;
import food.repository.IngredientRepository;
import food.request.FoodNoImageRequest;
import food.request.FoodRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class FoodService {

    private final FoodRepository foodRepository;

    private final IngredientRepository ingredientRepository;

    @Transactional
    public Food createFood(FoodRequest request) {
        var food = new Food();
        food.setName(request.name());
        food.setDescription(request.description());
        food.setPrice(request.price());

        try {
            food.setImage(request.image().getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Ingredient> ingredients = new ArrayList<>();

        if (request.ingredients() != null) {
            for (Integer ingredientId : request.ingredients()) {
                var ingredient = ingredientRepository.getById(ingredientId);
                ingredients.add(ingredient);
            }
        }

        food.setIngredients(ingredients);

        foodRepository.save(food);
        return foodRepository.getById(food.getId());
    }

    public List<Food> findAllFoods() {
        return foodRepository.findAllByOrderByIdAsc();
    }

    public Food findFoodByID(Integer id) {
        return foodRepository.getById(id);
    }

    public Food updateFood(Integer id, FoodNoImageRequest request) {
        var existingFood = findFoodByID(id);

        existingFood.setName(request.name());
        existingFood.setDescription(request.description());
        existingFood.setPrice(request.price());

        List<Ingredient> ingredients = new ArrayList<>();

        if (request.ingredients() != null) {
            for (Integer ingredientId : request.ingredients()) {
                var ingredient = ingredientRepository.getById(ingredientId);
                ingredients.add(ingredient);
            }
        }

        existingFood.setIngredients(ingredients);

        return foodRepository.save(existingFood);
    }
}
