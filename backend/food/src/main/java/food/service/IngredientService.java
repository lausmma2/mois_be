package food.service;

import food.domain.Ingredient;
import food.repository.FoodRepository;
import food.repository.IngredientRepository;
import food.request.IngredientRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
@Service
public class IngredientService {

    private final IngredientRepository ingredientRepository;
    private final FoodRepository foodRepository;

    public Ingredient createIngredient(IngredientRequest request) {
        var ingredient = new Ingredient();
        ingredient.setName(request.name());
        ingredient.setPrice(request.price());
        ingredient.setDescription(request.description());
        ingredient.setIsAvailable(request.isAvailable());
        ingredient.setIsAdditional(request.isAdditional());
        ingredient.setType(request.type());

        try {
            ingredient.setImage(request.image().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (request.foodIds() != null) {
            request.foodIds().stream()
                    .map(foodRepository::findById)
                    .filter(Optional::isPresent)
                    .forEach(f -> {
                        var food = f.get();
                        food.addIngredient(ingredient);
                        ingredient.addFood(food);
                    });
        }
        return ingredientRepository.save(ingredient);
    }

    public List<Ingredient> findAllIngredients() {
        return ingredientRepository.findAll();
    }

    public Ingredient findIngredientByID(Integer id) {
        return ingredientRepository.getById(id);
    }

    public Ingredient updateIngredient(Integer id, IngredientRequest request) {
        var oldIngredient = findIngredientByID(id);

        oldIngredient.setName(request.name());
        oldIngredient.setPrice(request.price());
        oldIngredient.setDescription(request.description());
        oldIngredient.setIsAvailable(request.isAvailable());
        oldIngredient.setIsAdditional(request.isAdditional());
        oldIngredient.setType(request.type());

        try {
            oldIngredient.setImage(request.image().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (request.foodIds() != null) {
            request.foodIds().stream()
                    .map(foodRepository::findById)
                    .filter(Optional::isPresent)
                    .forEach(f -> {
                        var food = f.get();
                        food.addIngredient(oldIngredient);
                        oldIngredient.addFood(food);
                    });
        }

        return ingredientRepository.save(oldIngredient);
    }
}
