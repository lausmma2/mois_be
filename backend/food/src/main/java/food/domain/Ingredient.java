package food.domain;

import food.dto.FoodDto;
import food.dto.IngredientDto;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "Ingredient")
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private BigDecimal price;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Boolean isAvailable;

    @Column(nullable = false)
    private Boolean isAdditional;

    @Lob
    @Column
    private byte[] image;

    @ManyToMany(mappedBy = "ingredients", cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    List<Food> foods = new ArrayList<>();

    @ManyToMany(mappedBy = "ingredients", cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    List<Food> orderedFoods = new ArrayList<>();

    @Column
    @Enumerated(EnumType.STRING)
    private IngredientType type;

    @Column
    @CreationTimestamp
    private Date createdAt;

    @Column
    @UpdateTimestamp
    private Date updatedAt;

    public void addFood(Food food) {
        getFoods().add(food);
    }

    public IngredientDto createDto() {
        return new IngredientDto(id, name, description, price, isAvailable, isAdditional, type, image);
    }
}
