package food.domain;

public enum IngredientType {
    VEGETABLE, MEAT, CHEESE, SAUCE
}
