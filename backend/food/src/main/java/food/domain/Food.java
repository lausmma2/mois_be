package food.domain;

import food.dto.FoodDto;
import food.dto.IngredientDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Food")
public class Food {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private BigDecimal price;

    @Lob
    @Column
    private byte[] image;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "foods_ingredients",
            joinColumns = {@JoinColumn(name = "food_id")},
            inverseJoinColumns = {@JoinColumn(name = "ingredient_id")}
    )
    List<Ingredient> ingredients = new ArrayList<>();

    @Column
    @CreationTimestamp
    private Date createdAt;

    @Column
    @UpdateTimestamp
    private Date updatedAt;

    public void addIngredient(Ingredient ingredient) {
        getIngredients().add(ingredient);
    }

    public FoodDto createDto() {
        List<IngredientDto> ingredientDtos = getIngredients().stream().map(Ingredient::createDto).collect(Collectors.toList());
        return new FoodDto(id, name, description, price, image, ingredientDtos);
    }
}