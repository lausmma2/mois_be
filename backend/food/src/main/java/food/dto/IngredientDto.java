package food.dto;

import food.domain.IngredientType;

import java.math.BigDecimal;

public record IngredientDto(
        Integer id,
        String name,
        String description,
        BigDecimal price,
        Boolean isAvailable,
        Boolean isAdditional,
        IngredientType type,
        byte[] image
) {
}
