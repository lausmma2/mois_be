package food.dto;

import java.math.BigDecimal;
import java.util.List;

public record FoodDto(
        Integer foodId,
        String name,
        String description,
        BigDecimal price,
        byte[] image,
        List<IngredientDto> ingredients
) {
}
