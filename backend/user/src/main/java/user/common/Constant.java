package user.common;

public class Constant {

    private Constant() {
        throw new UnsupportedOperationException("Class cannot be initialized");
    }

    /**
     * Authorization End point
     */
    public static final String JWT_SECRET = "MoIsJeSkvelejdasad212DEdawasd3D3WD444wvažečterřzVrsfFSDpAtOCka3332F3442567876543TFESDGFSDVGBDRVSCRTDVTCS5DFASA938REWUIRCwCWAT0Cůů-§.§,,.§¨¨ú)wewerr)cú-)ewrú-)ew.cú)r-wú)e/Wx";

    // HEADERS DEFAULTS
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "smart-todo-api";
    public static final String TOKEN_AUDIENCE = "smart-todo-app";

}
