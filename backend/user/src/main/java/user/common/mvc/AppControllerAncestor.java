package user.common.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import user.domain.AppUser;
import user.reposirory.AppUserRepository;

import java.util.Optional;

public abstract class AppControllerAncestor {

    @Autowired
    protected AppUserRepository userRepository;

    protected AppUser getUser(Authentication authentication) {
        Optional<AppUser> user = userRepository.findByUsername(authentication.getName());
        return user
                .orElseThrow(() -> new AccessDeniedException("User does not exist"));
    }
}
