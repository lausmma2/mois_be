package user.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import user.common.mvc.AppControllerAncestor;
import user.dto.RegistrationDto;
import user.service.AppUserService;

@RestController
@RequestMapping(value = "api/v1/user")
public class AppUserController extends AppControllerAncestor {

    public static final String AUTH_LOGIN_URL = "/api/v1/user/login";

    private final AppUserService userService;

    public AppUserController(AppUserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/registration")
    public @ResponseBody
    void userRegistration(@RequestBody RegistrationDto data) {

        userService.registerNewUser(data);
    }

    @GetMapping(value = "/auth")
    public @ResponseBody
    void authUser(Authentication auth) {
        getUser(auth);
    }
}
