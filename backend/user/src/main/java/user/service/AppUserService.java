package user.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import user.domain.AppUser;
import user.dto.RegistrationDto;
import user.reposirory.AppUserRepository;

import java.util.Optional;

@Service
public class AppUserService {

    private final AppUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public AppUserService(AppUserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void registerNewUser(RegistrationDto data) {
        AppUser user =
                AppUser.builder()
                        .username(data.getUsername())
                        .password(passwordEncoder.encode(data.getPassword()))
                        .firstName(data.getFirstName())
                        .lastName(data.getLastName())
                        .build();

        validNewUser(user);
        userRepository.save(user);
    }

    private void validNewUser(AppUser user) {
        if (StringUtils.isBlank(user.getUsername()))
            throw new IllegalArgumentException("Username is required");
        else if (!isUsernameUnique(user.getUsername()))
            throw new IllegalArgumentException("Username has been already used");

        if (StringUtils.isBlank(user.getPassword()))
            throw new IllegalArgumentException("Password is required");

    }

    private boolean isUsernameUnique(String username) {
        Optional<AppUser> user = userRepository.findByUsername(username);
        return user.isEmpty();
    }


}
